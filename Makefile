MAJOR = 1
MINOR = 6
REVISION = 0
VER = $(MAJOR).$(MINOR).$(REVISION)
PREFIX	=	/opt
bin-dir	=	/usr/local/bin
www-dir =	/var/www
DESTDIR =
all: 
install:
	python2 ./www/libre10_exec.py install --data-dir=${PREFIX} --bin-dir=${bin-dir} --www-dir=${www-dir} --dest-dir=${DESTDIR}
uninstall:
	rm -rf ${bin-dir}/libre10
	rm -rf ${www-dir}/libre10
	rm -rf ${PREFIX}/libre10

