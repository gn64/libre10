Installation
=================================

.. contents::

RHEL/Centos 7
---------------------------------
installation
##############

    #. Add `epel repository <https://fedoraproject.org/wiki/EPEL>`_
    #. Download home:gn64.repo from `libre10 repository <http://download.opensuse.org/repositories/home:/gn64/>`_ and move to /etc/yum.repos.d/
    #. Exec *yum install libre10* to install libre10.
    #. Start tomcat and apache server with *system start tomcat*   *system start httpd*
    #. Coufig firewalld and make *port 80*  accessible.

    .. code-block:: console

        $ sudo -s
        # cp home:gn64.repo /etc/yum.repos.d/
        # yum install libre10
        # systemctl start tomcat
        # systemctl start httpd

    .. warning::
        You need to exec commands listed below, if you want to start-up http/tomcat server after every server booting.

        .. code-block:: console

            # systemctl enable tomcat
            # systemctl enable httpd
Settings
#########
    Path : */etc/libre10.conf*

    ::

        [path]
        solrurl: http://localhost:8080/libre10

        dburl : /var/libre10/db/libre10.db

        #path containing pdf files.
        pdfpath : /var/libre10/pdf
        uploadpath : /var/libre10/pdf-upload

        [cache]
        ##Caching option
        cacheFolder : /var/libre10/cache
        #Save cached jpeg files extracted from pdf files.
        extendJpeg  : 1
        #Save width equals 1200 or 2400 jpeg files as cache.
        cache_width_1200  : 1
        cache_width_2400  : 1

Import pdf files
#################
    #. Move pdf files to `pdfpath` in conf(Default : */var/libre10/pdf* )
    #. exec *libre10 import*

Docker
------------------

Installation
############
    #. Install `Docker <https://www.docker.com/>`_
    #. Install libre10 as listed below.

    .. code-block:: console

        # Docker pull gn64/libre10
        # Docuer run --net=host -d -t libre10

Start/Stop
##########
    .. warning::
        You need to Docker commit before stopping Docker process to save data changes.

    .. code-block:: console
        First time initialize
        # Docker run --net=host -d -t <id>
        Start up
        # Docker start -i <id>
        Access to http://~~:8008

        Stop
        # Docker commit <id>
        # Docker stop <id>

Upload PDF files
#################
    #. Goto manager page by clicking `gear` icon in index page and move to upload tab.
    #. Drop down files and click upload button.
    #. Exec index command from Backup/Restore tab in Manager screen.

Mac OS X(homebrew)
------------------

Installation
#############
    #. Install `homebrew <http://brew.sh/>`_
    #. Install libre10 as listed below.

    .. code-block:: console

        $ brew tap gn64/libre10
        $ brew install gn64/libre10

    .. warning::
        Please reboot or re-login after installation.
Start/Stop
##########
    .. code-block:: console
        Start
        $ launchctl start org.rec10.libre10.wsgi
        $ launchctl start org.rec10.libre10.solr
        
        Stop
        $ launchctl stop org.rec10.libre10.wsgi
        $ launchctl stop org.rec10.libre10.solr
        
        Remove
        $ launchctl unload ~/Library/LaunchAgents/org.rec10.libre10.wsgi.plist
        $ launchctl unload ~/Library/LaunchAgents/org.rec10.libre10.solr.plist
        
Settings
###########
    Path : */usr/local/Cellar/libre10/1.5.0/www/libre10/libre10.conf*
    ::

        [path]
        solrurl: http://localhost:8983/solr/collection1

        dburl : /usr/local/var/libre10/db/libre10.db

        #path containing pdf files.
        pdfpath : /usr/local/var/libre10/pdf
        uploadpath : /usr/local/var/libre10/pdf-upload

        [cache]
        ##Caching option
        cacheFolder : /usr/local/var/libre10/cache
        #Save cached jpeg files extracted from pdf files.
        extendJpeg  : 1
        #Save width equals 1200 or 2400 jpeg files as cache.
        cache_width_1200  : 1
        cache_width_2400  : 1

Import pdf files
################
    #. Move pdf files to `pdfpath` in conf(Default : */usr/local/var/libre10/pdf* )
    #. exec *libre10 import*

Files
#######

::

 /usr/local/Cellar/libre10/
 └── 1.5.0
     ├── INSTALL_RECEIPT.json
     ├── License
     ├── bin
     ├── libre10
     │   ├── libre10
     │   ├── libre10-solr
     │   └── libre10-wsgi
     ├── libexec
     │   ├── bin
     │   │   ├── libre10
     │   │   ├── libre10-solr
     │   │   ├── libre10-wsgi
     │   │   ├── pilconvert.py
     │   │   ├── pildriver.py
     │   │   ├── pilfile.py
     │   │   ├── pilfont.py
     │   │   └── pilprint.py
     │   └── lib
     │       └── python2.7
     ├── readme.md
     └── www
         └── libre10
             ├── bottle.py
             ├── bottle.pyc
             ├── index.html
             ├── libre10-www.conf
             ├── libre10.conf
             ├── libre10.py
             ├── libre10.pyc
             ├── libre10.wsgi
             ├── libre10_exec.py
             ├── libre10_wsgi.py
             ├── login.html
             ├── manager.html
             └── view.html
 /usr/local/var/libre10/
 ├── cache
 │   ├── 1200
 │   │   └── <pdf title>
 │   ├── 2400
 │   │   └── <pdf title> 
 │   └── orig
 │       └── <pdf title>
 ├── db
 │   └── libre10.db
 ├── pdf
 ├── pdf-upload
 └── solr
