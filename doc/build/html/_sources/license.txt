Copyright/License
=========
.. contents::

Libre10
--------
    | Libre10 - pdf search tool-
    | Copyright (C) 2013-2014 yukikaze/gn64
    | License: MIT License

Dependent/Optional packages
-------------------------
Apache solr
############
    | `Apache solr <http://lucene.apache.org/solr/>`_
    | Apache solr is under Apache License Version 2.0.

Bootstrap
##########
    | `Bootstrap <http://getbootstrap.com/>`_
    | bootstrap is licensed under MIT license.

jQuery
#######
    | `jQuery <http://jquery.com/>`_
    | Copyright 2005, 2014 jQuery Foundation and other contributors
    | jQuery is provided under the MIT license.

jquery.touchswipe
##################
    | `jquery.touchswipe <http://labs.rampinteractive.co.uk/touchSwipe/>`_
    | Copyright (c) 2010 Matt Bryson
    | Dual licensed under the MIT or GPL Version 2 licenses.

knockout.js
############
    | `knockout.js <http://knockoutjs.com/>`_
    | Copyright (c) Steven Sanderson, the Knockout.js team, and other contributors
    | License: `MIT <http://www.opensource.org/licenses/mit-license.php>`_

knockout Mapping plugin
########################
    | `knockout Mapping plugin <https://github.com/SteveSanderson/knockout.mapping>`_
    | Knockout Mapping plugin v2.4.1
    | Copyright (c) 2013 Steven Sanderson, Roy Jacobs - http://knockoutjs.com/
    | License: `MIT <http://www.opensource.org/licenses/mit-license.php>`_

Jasny Bootstrap
################
    | `Jasny Bootstrap <http://jasny.github.io/bootstrap/>`_
    | The missing components for your favorite front-end framework.
    | Copyright 2013 Jasny BV under the Apache 2.0 license.

Bottle: Python Web Framework
#############################
    | `Bottle: Python Web Framework <http://bottlepy.org/docs/dev/index.html>`_
    | Copyright (c) 2014, Marcel Hellkamp.
    | License: MIT

docopt: creates beautiful command-line interfaces
##################################################
    | `docopt: creates beautiful command-line interfaces <https://github.com/docopt/docopt>`_
    | Copyright (c) 2012 Vladimir Keleshev, <vladimir@keleshev.com>
    | License: MIT (https://github.com/docopt/docopt/blob/master/LICENSE-MIT)

Python Paste
#############
    | `Python Paste <http://pythonpaste.org/>`_
    | Paste is distributed under the `MIT license <http://www.opensource.org/licenses/mit-license.php>`_.

anyjson: JSON library wrapper
##############################
    | `anyjson: JSON library wrapper <https://bitbucket.org/runeh/anyjson>`_
    | Rune F. Halvorsen <runefh@gmail.com>.
    | anyjson is distributed under a NEW BSD license.(https://bitbucket.org/runeh/anyjson/raw/0026a68c035696bdc8db8628e364139eba9a8ba8/LICENSE)

simplejson:JSON encoder and decoder
####################################
    | `simplejson:JSON encoder and decoder <http://simplejson.readthedocs.org/en/latest/>`_
    | Copyright (c) 2006 Bob Ippolito
    | simplejson is dual-licensed software. It is available under the terms of the MIT license, or the Academic Free License version 2.1.
