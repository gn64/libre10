.. libre10 documentation master file, created by
   sphinx-quickstart on Fri Dec 05 22:50:16 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to libre10 manual
===================================
Homepage:
    http://libre10.rec10.org/
Git:
    https://bitbucket.org/gn64/libre10
Contents:

.. toctree::
    :maxdepth: 3

    top_en
    top_ja

    license


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

