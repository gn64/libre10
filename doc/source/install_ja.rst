Libre10のインストール/初期設定
=================================

.. contents::

RHEL/Centos 7
---------------------------------

インストール
############

    #. `epel <https://fedoraproject.org/wiki/EPEL/ja>`_ をリポジトリに追加します。
    #. `libre10リポジトリ <http://download.opensuse.org/repositories/home:/gn64/>`_ からhome:gn64.repoをダウンロード、/etc/yum.repos.d/へ移動します。
    #. *yum install libre10* を実行し、libre10をインストールします。
    #. *system start tomcat*   *system start httpd* でtomcat/apacheを起動します。
    #. firewalldを設定し、*port 80* を外部からアクセス可能にします。

    .. code-block:: console

        $ sudo -s
        # cp home:gn64.repo /etc/yum.repos.d/
        # yum install libre10
        # systemctl start tomcat
        # systemctl start httpd

    .. warning::
        毎回起動時にhttp/tomcatを起動するには下記を実行してください。

        .. code-block:: console

            # systemctl enable tomcat
            # systemctl enable httpd

初期設定
#########
    設定ファイル : */etc/libre10.conf*

    ::

        [path]
        solrurl: http://localhost:8080/libre10

        dburl : /var/libre10/db/libre10.db

        #path containing pdf files.
        pdfpath : /var/libre10/pdf
        uploadpath : /var/libre10/pdf-upload

        [cache]
        ##Caching option
        cacheFolder : /var/libre10/cache
        #高速化を図るために、pdfからjpegをキャッシュとして保存します。
        extendJpeg  : 1
        #横幅が1200もしくは2400のjpegをキャッシュとして保存します。
        cache_width_1200  : 1
        cache_width_2400  : 1

文章取り込み
#############
    #. 設定ファイルのpdfpath(標準では */var/libre10/pdf* )へpdf fileを移動します。
    #. *libre10 import* を実行します。

Debian/Ubuntu
---------------------------------
インストール
##############

    #. Gitリポジトリから最新のLibre10をダウンロードします.
    #. 依存パッケージをインストールします。
    #. Libre10 ユーザー/グループを追加します
    #. Libre10ユーザーで`libre10-solr`及び`libre10-wsgi`を実行し、サーバーを立ち上げます。
    #. `libre10 import`を実行し、Libre10のDBを初期化します。.
    #. http://localhost:8008 もしくは http://ip:8008 へアクセスしてください。

    .. code-block:: console

        $ mkdir libre10
        $ cd libre10
        $ git clone https://bitbucket.org/gn64/libre10.git
        $ cd libre10
        $ sudo -s
        # apt-get -y install imagemagick graphicsmagick poppler-utils libjpeg-progs python2.7
        # apt-get -y install python-requests python-docopt python-crypto python-pillow python-sqlalchemy python-anyjson
        # apt-get -y install python-simplejson python-paste openjdk-7-jre-headless
        # bash ./install.sh install
        # groupadd libre10
        # useradd -g libre10 libre10
        # chown -R libre10:libre10 /var/www/libre10 && chown -R libre10:libre10 /opt/libre10 && chmod 777 /usr/local/bin/libre10*
        # su libre10
        $ libre10-wsgi&
        $ libre10-solr&
        $ libre10 import

    .. warning::
You need to exec commands listed below, if you want to start-up http/tomcat server after every server booting.

    .. code-block:: console

        # su libre10
        $ libre10-wsgi&
        $ libre10-solr&
        $ libre10 import

Import pdf files
#################
    #. Move pdf files to `pdfpath` in conf(Default : */opt/libre10/pdf* )
    #. exec *libre10 import*

Docker
------------------

インストール
############
    #. `Docker <https://www.docker.com/>`_ をインストールします
    #. 下記に従いlibre10をインストールします。

    .. code-block:: console

        # Docker pull gn64/libre10
        # Docker run --net=host -d -t libre10

起動/終了
##########
    .. warning::
        Dockerではデータを損なうためにDocker commitを行う必要があります。そうでないばあい、毎度データがリセットされます。

    .. code-block:: console
        起動<初回>
        # Docker run --net=host -d -t <id>
        起動
        # Docker start -i <id>
        アクセス
            http://~~:8008にアクセスします。

        終了
        # Docker commit <id>
        # Docker stop <id>

文章取り込み
#############
    #. WebからManager(indexページの歯車のアイコン), Uploadタブにてアップロード画面へ移動
    #. 取り込みたいファイルをドロップし、アップロードします。
    #. ManagerのBackup/Restore画面からIndexを実行します。

Mac OS X(homebrew)
------------------

インストール
############
    #. `homebrew <http://brew.sh/index_ja.html>`_ をインストールします
    #. 下記に従いlibre10をインストールします。
    
    .. code-block:: console

        $ brew tap gn64/libre10
        $ brew install gn64/libre10

    .. warning::
        インストール後には再度ログインもしくは再起動して下さい。
起動/終了
##########
    .. warning::
        Libre10は初期状態では自動起動します。
        
    .. code-block:: console
        起動
        $ launchctl start org.rec10.libre10.wsgi
        $ launchctl start org.rec10.libre10.solr
        
        終了
        $ launchctl stop org.rec10.libre10.wsgi
        $ launchctl stop org.rec10.libre10.solr
        
        起動スクリプトの削除
        $ launchctl unload ~/Library/LaunchAgents/org.rec10.libre10.wsgi.plist
        $ launchctl unload ~/Library/LaunchAgents/org.rec10.libre10.solr.plist
        
初期設定
###########
    設定ファイル */usr/local/Cellar/libre10/1.5.0/www/libre10/libre10.conf*
    ::

        [path]
        solrurl: http://localhost:8983/solr/collection1

        dburl : /usr/local/var/libre10/db/libre10.db

        #path containing pdf files.
        pdfpath : /usr/local/var/libre10/pdf
        uploadpath : /usr/local/var/libre10/pdf-upload

        [cache]
        ##Caching option
        cacheFolder : /usr/local/var/libre10/cache
        #高速化を図るために、pdfからjpegをキャッシュとして保存します。
        extendJpeg  : 1
        #横幅が1200もしくは2400のjpegをキャッシュとして保存します。
        cache_width_1200  : 1
        cache_width_2400  : 1

文章取り込み
#############
    #. 設定ファイルのpdfpath(標準では */usr/local/var/libre10/pdf* )へpdf fileを移動します。
    #. *libre10 import* を実行します。

ファイル一覧
#############

::

 /usr/local/Cellar/libre10/
 └── 1.5.0
     ├── INSTALL_RECEIPT.json
     ├── License
     ├── bin
     ├── libre10
     │   ├── libre10
     │   ├── libre10-solr
     │   └── libre10-wsgi
     ├── libexec
     │   ├── bin
     │   │   ├── libre10
     │   │   ├── libre10-solr
     │   │   ├── libre10-wsgi
     │   │   ├── pilconvert.py
     │   │   ├── pildriver.py
     │   │   ├── pilfile.py
     │   │   ├── pilfont.py
     │   │   └── pilprint.py
     │   └── lib
     │       └── python2.7
     ├── readme.md
     └── www
         └── libre10
             ├── bottle.py
             ├── bottle.pyc
             ├── index.html
             ├── libre10-www.conf
             ├── libre10.conf
             ├── libre10.py
             ├── libre10.pyc
             ├── libre10.wsgi
             ├── libre10_exec.py
             ├── libre10_wsgi.py
             ├── login.html
             ├── manager.html
             └── view.html
 /usr/local/var/libre10/
 ├── cache
 │   ├── 1200
 │   │   └── <pdf title>
 │   ├── 2400
 │   │   └── <pdf title> 
 │   └── orig
 │       └── <pdf title>
 ├── db
 │   └── libre10.db
 ├── pdf
 ├── pdf-upload
 └── solr

