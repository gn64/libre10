Libre10 web tool usage
========================

.. contents::

Access
-------------------
    Access to http://localhost/libre10/ or http://localhost:8008/ from installed computer.
    .. warning::
        Default port was changed from 8000 to 8008 in version 1.5
Login
-------------------


Add user
**************
    .. image:: ./img/libre10-login-signin.jpg
        :width: 30%
    .. image:: ./img/libre10-login-signup.jpg
        :width: 30%
    .. warning::
        On first access, libre10 might moves to sign up screen. If not please click *>>Sign up(Log-out)* button.

    Input user name and password and click *>>Sign in* button.
    Then Libre10 moves to login screen. Please input user name and password to login.

Search screen
--------

top
*********

    .. image:: ./img/libre10-top.jpg
        :width: 50%

    1. Input search text
    2. Exec search on libre10 db
    3. Exec search on google
    4. Go to manager
    5. Help (this document)
    6. Log out
    7. Limit search on selected genre.

Search result
********

    .. image:: ./img/libre10-top-2.jpg
        :width: 50%

    1. Book(pdf file) titles
    2. Number of matches
    3. Go to top
    4. View this page
    5. Book title list

View screen
---------

    .. toctree::

List of functions
********
    .. image:: ./img/libre10-view.jpg
        :width: 50%

    1. Go to search screen
    2. Last page
    3. Last result page
    4. Page navigation
    5. Jump to page
    6. Page number (If diff number of page was set, page number in book is also shown.)
    7. Book title
    8. Go to help (this document)
    9. Go to pdf setting screen.
    10. Next result page
    11. None
    12. Next page
    13. Change resolution (page size)
    14. Enable/Disable highlighting


Shortcut keys
************************

Key map
############
    .. image:: ./img/key-all.png
        :width: 50%

    Libre10 have three key styles.
    FPS like mode using WASD keys, VIM like mode using hjkl keys and arrow keys mode.

Key binds
##########

    #. Go to last/next page

        .. image:: ./img/key-navigation.png
            :width: 50%
    #. Go to last/next result page

        .. image:: ./img/key-result-navigation.png
            :width: 50%

    #. Expand/Shrink page (available in 2page mode.)

        .. image:: ./img/key-page-size.png
            :width: 50%

    #. Number key for VIM style
        Many commands accept multiple number key input before the command. It works as if the command execute the number times.
        For example, input 240 and *G key* will go to 240 page.

Manager screen
--------

    .. image:: ./img/libre10-manager.jpg
        :width: 50%

    In manager screen, you can change pdf settings like genre, manage users, upload pdf files and update/clear index.

    1. PDF List
        The list of pdf files. You can set genre by inputting in Genre form and click next + button or inputting multiple Genre form and click *+ALL+* button in top line.
        You can set the diff number of page number in the pdf file and page number in the book or index page number by clicking SET button in SpD/Index column.
    2. Genre List
        The list of genres.
    3. User List
        You can manage users. If you want to make new user enable to login, you need to *Authorize*  the user by authorized user.
    4. Uploader
        Upload pdf file.
    5. Status/Backup
        Clear old db or index pdf files.
        Backup/restore booktitle db.
        .. warning::
            Index via web is only available in Debian/Docker. In other platform, you need to index from console by executing *libre10 import*.
