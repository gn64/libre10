%define debug_package %{nil}
%define _unpackaged_files_terminate_build 0
%define _rpmfilename %%{ARCH}/%%{NAME}-%%{VERSION}-%%{RELEASE}.%%{DISTRIBUTION}.%%{ARCH}.rpm

%define tomcat /etc/tomcat
%define tomcat_lib /usr/share/java/tomcat
%define tomcat_conf /etc/tomcat/Catalina/localhost
%define http_dir /var/www/html

Summary:	libre10 - yet another hoge -
Name:		libre10
Version:	1.8.0
Release:	1
Distribution:	rec10
Group:		Applications/Multimedia
Source:		%{name}-%{version}.tar.gz
Vendor:		rec10
License:	MIT License
URL:		http://www.rec10.org/
BuildRoot:	%{_tmppath}/%{name}-%{version}-buildroot
Requires:	python > 2.6  GraphicsMagick ImageMagick poppler-utils
Requires:	tomcat >= 7 java >= 1:1.7.0 jre >= 1.7.0
Requires:	/usr/bin/jpegtran
Requires:   libre10-www = %{version}-%{release} libre10-selinux = %{version}-%{release}
AutoReqProv:	no
Provides:	libre10 = %{version}-%{release}

%description
libre10 is a all text search and viewing tool for pdf files.

%package www
Summary: 	www files for libre10
Group: 		Applications/Multimedia
Requires:	httpd mod_wsgi
Requires:   python > 2.6 python-requests python-docopt python-crypto >= 2.6 python-imaging python-sqlalchemy >= 0.7.5
Requires:   python-anyjson python-simplejson
Requires(pre):   libre10 = %{version}-%{release}
AutoReqProv:	no
%description www
libre10-www

%package selinux
Summary: 	selinux related files
Requires(pre):	libre10 = %{version}-%{release} libre10-www = %{version}-%{release}
Requires(post):	policycoreutils-python
Requires(postun):	policycoreutils-python
AutoReqProv:	yes
%description selinux
This rpm change selinux bool option httpd_can_network_connect to 1.


%prep
rm -rf %{buildroot}

%setup

%build

%pre

%install
%__mkdir -p %{buildroot}/usr/local/bin
%__mkdir -p %{buildroot}/usr/lib/libre10
%__mkdir -p %{buildroot}/var/log
%__mkdir -p %{buildroot}/var/libre10/solr
%__mkdir -p %{buildroot}/var/libre10/db
%__mkdir -p %{buildroot}/var/libre10/pdf
%__mkdir -p %{buildroot}/var/libre10/pdf-upload
%__mkdir -p %{buildroot}/var/libre10/cache/2400
%__mkdir -p %{buildroot}/var/libre10/cache/1200
%__mkdir -p %{buildroot}/var/libre10/cache/orig
%__mkdir -p %{buildroot}/var/libre10/cache/text
%__mkdir -p %{buildroot}/etc/httpd/conf.d/
%__mkdir -p %{buildroot}%{http_dir}/libre10/help
%__mkdir -p %{buildroot}%{http_dir}/libre10/images
%__mkdir -p %{buildroot}%{http_dir}/libre10/views
%__mkdir -p %{buildroot}%{tomcat_lib}/
%__mkdir -p %{buildroot}%{tomcat_conf}/
cd bin
%__cp libre10 %{buildroot}/usr/local/bin

#TODO: update solr version
cd ../solr-5.1.0
%__cp ./server/lib/ext/* %{buildroot}%{tomcat_lib}
%__cp ./example/resources/log4j.properties %{buildroot}%{tomcat_lib}
%__cp -r ./* %{buildroot}/var/libre10/solr
cd ../solr
%__cp  -r ./libre10 %{buildroot}/var/libre10/solr/server/solr/
cd ../conf
%__cp ./libre10.rpm.conf %{buildroot}/etc/libre10.conf
%__cp ./libre10.xml %{buildroot}%{tomcat_conf}/
%__cp ./libre10.db %{buildroot}/var/libre10/db/libre10.db
cd ../www
%__cp libre10-www.conf %{buildroot}/etc/httpd/conf.d/libre10.conf
%__cp *.html %{buildroot}%{http_dir}/libre10/
%__cp *.py %{buildroot}%{http_dir}/libre10/
%__cp *.wsgi %{buildroot}%{http_dir}/libre10/
%__cp -r ./images/* %{buildroot}%{http_dir}/libre10/images
%__cp -r ./views/* %{buildroot}%{http_dir}/libre10/views
cd ../doc/build/html
%__cp -r * %{buildroot}%{http_dir}/libre10/help
cd ../../../
%post
%clean
rm -rf %{buildroot}

%post selinux
semanage fcontext -a -t httpd_sys_rw_content_t '/var/libre10/cache(/.*)?' 2>/dev/null || :
semanage fcontext -a -t httpd_sys_rw_content_t '/var/libre10/db(/.*)?' 2>/dev/null || :
semanage fcontext -a -t httpd_sys_rw_content_t '/var/libre10/pdf-upload(/.*)?' 2>/dev/null || :
semanage fcontext -a -t httpd_sys_content_t '/var/libre10/pdf(/.*)?' 2>/dev/null || :
semanage fcontext -a -t httpd_sys_script_exec_t '%{http_dir}/libre10(/.*)?' 2>/dev/null || :
restorecon -R /var/libre10/cache || :
restorecon -R /var/libre10/db || :
restorecon -R /var/libre10/pdf-upload || :
restorecon -R /var/libre10/pdf || :
restorecon -R %{http_dir}/libre10 || :
setsebool -P httpd_can_network_connect 1
/usr/local/bin/libre10
/usr/local/bin/libre10 update
rm -f /tmp/libre10

%postun selinux
if [ $1 -eq 0 ] ; then
	semanage fcontext -d -t httpd_sys_rw_content_t '/var/libre10/cache(/.*)?' 2>/dev/null || :
	semanage fcontext -d -t httpd_sys_rw_content_t '/var/libre10/db(/.*)?' 2>/dev/null || :
	semanage fcontext -d -t httpd_sys_rw_content_t '/var/libre10/pdf-upload(/.*)?' 2>/dev/null || :
	semanage fcontext -d -t httpd_sys_content_t '/var/libre10/pdf(/.*)?' 2>/dev/null || :
	semanage fcontext -d -t httpd_sys_script_exec_t '%{http_dir}/libre10(/.*)?' 2>/dev/null || :
	setsebool -P httpd_can_network_connect 0
fi

%files
%defattr(-,root,root)
%attr(755,root,root)/usr/local/bin/libre10
/usr/lib/libre10/
%{tomcat_lib}/jcl-over-slf4j-1.7.7.jar
%{tomcat_lib}/jul-to-slf4j-1.7.7.jar
%{tomcat_lib}/log4j-1.2.17.jar
%{tomcat_lib}/slf4j-api-1.7.7.jar
%{tomcat_lib}/slf4j-log4j12-1.7.7.jar
%{tomcat_lib}/log4j.properties
%attr(-,tomcat,tomcat)/var/libre10/solr/
%{tomcat_conf}/libre10.xml
%config(noreplace)/etc/libre10.conf
%attr(777,root,root)/var/libre10/cache
%dir %attr(777,root,root)/var/libre10/db
%attr(777,root,root)/var/libre10/pdf
%attr(777,root,root)/var/libre10/pdf-upload
%config(noreplace)%attr(777,root,root)/var/libre10/db/libre10.db

%files www
%defattr(-,root,root,-)
%dir %{http_dir}/libre10/
/etc/httpd/conf.d/libre10.conf
%attr(755,root,users)%{http_dir}/libre10/*.py*
%attr(755,root,users)%{http_dir}/libre10/*.wsgi
%attr(644,root,users)%{http_dir}/libre10/*.html
%attr(755,root,users)%{http_dir}/libre10/help
%attr(755,root,users)%{http_dir}/libre10/images
%attr(755,root,users)%{http_dir}/libre10/views


%files selinux

%changelog
* Tue May 05 2015 gn64 <gn64@rec10.org>
- Add icons.
* Thu Nov 13 2014 gn64 <gn64@rec10.org>
- Implement anyjson/simplejson for json serializing.
* Tue Oct 28 2014 gn64 <gn64@rec10.org>
- Add next/last search result page button.
* Tue Oct 21 2014 gn64 <gn64@rec10.org>
- Optimize jpeg cache size for iPad/Mac/PCs.
* Mon Oct 13 2014 gn64 <gn64@rec10.org>
- implement login option.
* Sat Sep 27 2014 gn64 <gn64@rec10.org>
- implement MVVM model html/script files.
* Fri Aug 29 2014 gn64 <gn64@rec10.org>
- change db file to noreplace option.
* Sun Jul 13 2014 gn64 <gn64@rec10.org>
- remove easy_install dependent packages.
* Sat Jul 12 2014 gn64 <gn64@rec10.org>
- remove CPAN related packages.
* Fri Jul 04 2014 gn64 <gn64@rec10.org>
- add selinux related settings.
* Sat May 17 2014 gn64 <gn64@rec10.org>
- remove obsolute files list.
* Tue Apr 08 2014 gn64 <gn64@rec10.org>
- update solr to 4.7.1
* Sun Apr 06 2014 gn64 <gn64@rec10.org>
- make spec file.
