#!/bin/sh
# Create RPM files for rec10
VERSION="1.8.0"
# TODO: fix version number
RPMBUILD=`mktemp -d $HOME/rpmbuild.XXXXXX`
#RPMMACRO=`mktemp    $HOME/.rpmmacros.XXXXXX`
echo "Build directory $RPMBUILD"
mkdir -p $RPMBUILD/{BUILD,RPMS,SOURCES,SPECS,SRPMS}
mkdir libre10-$VERSION
cp -r ./conf libre10-$VERSION/
cp -r ./bin libre10-$VERSION/
cp -r ./doc libre10-$VERSION/
#TODO: update solr version
cp -r ./solr-5.1.0 libre10-$VERSION/
cp -r ./solr libre10-$VERSION/
cp -r ./www libre10-$VERSION/
cp -r ./debian libre10-$VERSION/
cp ./Makefile libre10-$VERSION/
cp ./readme.md libre10-$VERSION/
cp ./License libre10-$VERSION/
tar zcvf libre10-$VERSION.tar.gz ./libre10-$VERSION
cp libre10-$VERSION.tar.gz $RPMBUILD/SOURCES
cp libre10.spec $RPMBUILD/SPECS
mv ~/.rpmmacros ~/.rpmmacros.old
echo "%_topdir $RPMBUILD" > ~/.rpmmacros
rpmbuild -ba "$RPMBUILD/SPECS/libre10.spec"
cp $RPMBUILD/RPMS/noarch/* .
cp $RPMBUILD/RPMS/`arch`/* .
cp $RPMBUILD/SRPMS/* .
mv ~/.rpmmacros.old ~/.rpmmacros
rm -r libre10-$VERSION/
#rm libre10-$VERSION.tar.gz
echo 'Removing build directory'
rm -r $RPMBUILD
#rm $RPMMACRO

