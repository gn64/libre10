#Libre10
***

## Overview
Libre10 is a powerful pdf indexing and searching tool based on http server.

## Demo
![top image](https://bytebucket.org/gn64/libre10/raw/8e81021c28398dfbaf8680eb233883eee6ac87a5/images/libre10-1.6.0.gif)
## Requirement
* GraphicsMagick/ImageMagick
* poppler-utils
* java >= 1.7.0
* python >= 2.6
* python-requests
* python-docopt
* python-crypto >= 2.6
* python-pillow(python-imaging)
* python-sqlalchemy >= 0.7.5
* python-anyjson
* (Optional) python-simplejson
* (Optional) python-paste
* (Optional) jpegtran

## Usage
### 1.Move pdf files
`cp *.pdf /var/libre10/pdf`
### 2.Index
`libre10 index`
### 3.Search on web
Access to http://localhost/libre10 or http://localhost:8008/

### 4.indexer
```
'libre10' import   
'libre10' index   
'libre10' install [--data-dir=<data_dir>] [--bin-dir=<bin_dir>] [--www-dir=<www_dir>] [--disable-jetty-bin] [--disable-wsgi-bin]   
'libre10' remove [--data-dir=<data_dir>] [--bin-dir=<bin_dir>] [--www-dir=<www_dir>]   
'libre10' update   
'libre10' -h | --help   
'libre10' --version   
``` 

## Install
### RHEL/Centos 6 or 7
1.Enable/Install [epel](https://fedoraproject.org/wiki/EPEL)   
2.Go to [http://download.opensuse.org/repositories/home:/gn64/](download.opensuse.org/repositories/home:/gn64/) and download home:gn64.repo to /etc/yum.repos.d/  
3.install libre10 and startup http/tomcat server   
```
# cp home:gn64.repo /etc/yum.repos.d/   
# yum install libre10  
(RHEL/Centos 6)# /etc/init.d/tomcat start  
(RHEL/Centos 6)# /etc/init.d/httpd start  
(RHEL/Centos 7)# systemctl start tomcat  
(RHEL/Centos 7)# systemctl start httpd  
```

### Mac OS X (homebrew)
```
$ brew tap gn64/libre10  
$ brew install gn64/libre10  
```
You need to restart or re-login after installation.

## Contribution
1. Fork it ( https://bitbucket.org/gn64/libre10/fork )
2. Create your feature branch (git checkout -b my-new-feature)
3. Commit your changes (git commit -am 'Add some feature')
4. Push to the branch (git push origin my-new-feature)
5. Create new Pull Request

## License
Libre10 - pdf search tool-  
Copyright (C) 2013-2015 yukikaze/gn64
License: MIT License

### [Apache solr](http://lucene.apache.org/solr/)  
> Apache solr is under Apache License Version 2.0.

### [Bootstrap](http://getbootstrap.com/) 
> bootstrap is licensed under MIT license.

### [jQuery](http://jquery.com/)
> Copyright 2005, 2014 jQuery Foundation and other contributors
> jQuery is provided under the MIT license.

### [Hammer.js](http://hammerjs.github.io/)
> Copyright (C) 2011-2014 by Jorik Tangelder (Eight Media)
> The MIT License (MIT)

### [knockout.js](http://knockoutjs.com/)
> Copyright (c) Steven Sanderson, the Knockout.js team, and other contributors  
> License: MIT (http://www.opensource.org/licenses/mit-license.php)

### [knockout Mapping plugin](https://github.com/SteveSanderson/knockout.mapping)
> Knockout Mapping plugin v2.4.1  
> Copyright (c) 2013 Steven Sanderson, Roy Jacobs - http://knockoutjs.com/  
> License: MIT (http://www.opensource.org/licenses/mit-license.php)

### [Jasny Bootstrap](http://jasny.github.io/bootstrap/)
> The missing components for your favorite front-end framework.
> Copyright 2013 Jasny BV under the Apache 2.0 license.

### [Bottle: Python Web Framework 0.12.7](http://bottlepy.org/docs/dev/index.html)
> Copyright (c) 2014, Marcel Hellkamp.  
> License: MIT

### [docopt: creates beautiful command-line interfaces ](https://github.com/docopt/docopt)
> Copyright (c) 2012 Vladimir Keleshev, <vladimir@keleshev.com>  
> License: MIT (https://github.com/docopt/docopt/blob/master/LICENSE-MIT)  

### [Python Paste](http://pythonpaste.org/)
> Paste is distributed under the MIT license

### [anyjson: JSON library wrapper](https://bitbucket.org/runeh/anyjson)
> Rune F. Halvorsen <runefh@gmail.com>.  
> anyjson is distributed under a NEW BSD license.(https://bitbucket.org/runeh/anyjson/raw/0026a68c035696bdc8db8628e364139eba9a8ba8/LICENSE)  

### [simplejson:JSON encoder and decoder](http://simplejson.readthedocs.org/en/latest/)
> Copyright (c) 2006 Bob Ippolito  
> simplejson is dual-licensed software. It is available under the terms of the MIT license, or the Academic Free License version 2.1.

### [Modrnizr.js](https://modernizr.com/)
> Copyright (c) 2006 Bob Ippolito  
> License: MIT

### [js-cookie](https://github.com/js-cookie/js-cookie)
> Copyright 2014 Klaus Hartl
> License: MIT

## History
***
* 16/04/05 fix Safari localStrage related errors.
* 15/07/13 add rest search url like libre10.wsgi/search/<search_text>/
* 15/05/03 fix login bug.
* 15/01/01 add position text cache.
* 14/12/28 add unsharp option for jpeg cache.
* 14/12/23 implement DB backup function.
* 14/12/16 v1.5: change default wsgi serve from cherrypy to paste
* 14/12/15 change default port from 8000 to 8008
* 14/12/12 add primary support for debian8/Docker
* 14/12/02 v1.4: add searched word position highlighter
* 14/11/25 now libre10 support pdf files without images
* 14/11/26 change license to MIT License.
* 14/11/15 change solr highlighter.
* 14/11/04 update bootstrap version 3.3.0.
* 14/11/03 add python-sqlalchemy for DB loading.
* 14/10/29 add python-docopt for indexer.
* 14/10/09 change cgi to wsgi mode.
* 14/10/03 add bottle python web framework 0.12.7
* 14/09/20 add knockout.js and knockout mapping plugin to implement ajax page loading.
* 14/07/30 update bootstrap
* 14/07/27 update solr packages.
* 14/06/14 add jquery.touchswipe to implement swipe page navigation.
* 14/05/03 remove mobile mode and replace desktop js engine kendo UI to jquery + bootstrap.
* 14/03/26 implement sanitize for input text.
* 14/01/05 add readme.md
