#!/usr/bin/python
# -*- coding:utf-8 -*-

from __future__ import print_function, unicode_literals
import os
import sqlite3
import hashlib
import datetime
import subprocess
import shutil
import time
import ConfigParser

import sqlalchemy
from sqlalchemy.ext import declarative
from sqlalchemy import Column, Integer, String, DateTime, Float, func, ForeignKey
from sqlalchemy.orm import relation, backref

Base = declarative.declarative_base()
import requests
import anyjson

try:
    from PIL import Image
except:
    import Image


# Libre10 web based pdf search engine
# Copyright (C) 2013-15 yukikaze



class Config():
    def __init__(self, path=""):
        os.chdir(os.path.dirname(__file__))
        if len(path) == 0:
            if os.path.exists("./libre10.conf"):
                conf_path = "./libre10.conf"
            elif os.path.exists("../libre10.conf"):
                conf_path = "../libre10.conf"
            elif os.path.exists("../conf/libre10.conf"):
                conf_path = "../conf/libre10.conf"
            elif os.path.exists("/usr/local/etc/libre10.conf"):
                conf_path = "/usr/local/etc/libre10.conf"
            else:
                conf_path = "/etc/libre10.conf"
        else:
            conf_path = path
        self.conf_path = conf_path
        self.conf = ConfigParser.SafeConfigParser()
        self.conf.read(conf_path)

    def get(self, section, option, default=""):
        try:
            return self.conf.get(section, option)
        except:
            return default

    def getboolean(self, section, option):
        return self.conf.getboolean(section, option)

    def set(self, section, option, value):
        return self.conf.set(section, option, value)

    def write(self, path):
        f = open(path, 'w')
        self.conf.write(f)
        f.close()


class Libre10db():
    def open(self):
        self.session_maker = sqlalchemy.orm.scoped_session(
            sqlalchemy.orm.sessionmaker(bind=self.engine, autoflush=False))
        self.session = self.session_maker()

    def close(self):
        self.session_maker.remove()
        self.session.close()

    def row2dict(self, row):
        if row:
            ret = {}
            for column in row.__table__.columns:
                rtemp = getattr(row, column.name)
                if rtemp is not None:
                    if type(rtemp) == datetime.datetime:
                        ret[column.name] = rtemp.strftime("%Y-%m-%d %H:%M:%S")
                    else:
                        ret[column.name] = rtemp
            return ret
        else:
            return {}

    def rows2dict(self, rows):
        ret = []
        for i in rows:
            ret.append(self.row2dict(i))
        return ret

    def dict_factory(self, cursor, row):
        '''return SQL as dict'''
        d = {}
        for idx, col in enumerate(cursor.description):
            d[col[0]] = row[idx]
        return d

    def __init__(self, sqlpathstr):
        self.solr_version = "1.5"
        # TODO: update solr_schema_version
        self.engine = sqlalchemy.create_engine('sqlite:///' + sqlpathstr)
        # print('sqlite://' + sqlpathstr)
        self.sqlpath = sqlpathstr
        # Session = sqlalchemy.orm.sessionmaker(bind=self.engine)
        self.session_maker = sqlalchemy.orm.scoped_session(
            sqlalchemy.orm.sessionmaker(bind=self.engine, autoflush=False))
        self.session = self.session_maker()
        Base.metadata.create_all(self.engine)
        self.session_maker.remove()

    def __enter__(self):
        self.open()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()

    class db_pdffile(Base):

        __tablename__ = 'pdffile'
        id = Column(String, primary_key=True, index=True)
        path = Column(String, nullable=False)
        page = Column(Integer, default=0, nullable=False, index=True)
        title = Column(String, nullable=False)
        title_id = Column(String, nullable=False, index=True)
        part = Column(Integer, default=0, nullable=False, index=True)
        genre = Column(String, default="", nullable=False)
        startpage = Column(Integer, default=0, nullable=False, index=True)
        endpage = Column(Integer, default=0, nullable=False, index=True)
        showpagediff = Column(Integer, default=0, nullable=False)
        indexpage = Column(Integer, default=0, nullable=False)
        changed = Column(Integer, default=0, nullable=False)

    class db_pdffile_user(Base):

        __tablename__ = 'pdffile_user'
        id = Column(Integer, primary_key=True)
        username = Column(String, unique=False, nullable=False)
        page = Column(Integer, default=0, nullable=False, index=True)
        title_id = Column(String, nullable=False, index=True)
        count = Column(Integer, default=0, nullable=False, index=True)
        last = Column(DateTime, default=datetime.datetime.now(), nullable=False)

    class db_genre(Base):

        __tablename__ = 'genre'
        __table_args__ = (
            dict(
                sqlite_autoincrement=True))
        id = Column(Integer, primary_key=True)
        genre = Column(String, nullable=False)
        title_id = Column(String, nullable=False)
        title = Column(String, nullable=False)

    class db_user(Base):

        __tablename__ = 'user'
        id = Column(Integer, primary_key=True)
        username = Column(String, unique=True, nullable=False)
        sugar = Column(String, nullable=False)
        password = Column(String, nullable=False)
        lastlogin = Column(String, default="", nullable=False)
        auth = Column(Integer, nullable=False)

    class db_session(Base):

        __tablename__ = 'session'
        id = Column(Integer, primary_key=True)
        username = Column(String, nullable=False, default="")
        session = Column(String, unique=True)
        salt = Column(String, nullable=False)
        ip = Column(String, nullable=False)
        result = Column(Integer, nullable=False)
        available = Column(DateTime, default=datetime.datetime.now())

    class db_key(Base):

        __tablename__ = 'key'
        id = Column(Integer, primary_key=True)
        private = Column(String, nullable=False)
        public = Column(String, nullable=False)
        ip = Column(String, nullable=False)
        available = Column(DateTime, default=datetime.datetime.now())

    class db_status(Base):

        __tablename__ = 'status'
        id = Column(Integer, primary_key=True)
        # TODO: update libre10 version
        libre10_version = Column(String, nullable=False, default='1.8.0')
        solr_version = Column(String, nullable=False, default='1.5')
        is_indexed = Column(Integer, nullable=False, default='0')

    ## login/seesion

    def check_first_user(self):
        return self.session.query(self.db_user).count() < 1

    def check_session(self, session_string):
        return self.session.query(self.db_session).filter(
            self.db_session.session == session_string,
            self.db_session.available > datetime.datetime.today()
        ).count() > 0

    def check_session_authorized(self, session_string):
        return self.session.query(self.db_session).filter(
            self.db_session.session == session_string,
            self.db_session.available > datetime.datetime.today(),
            self.db_session.result == 1
        ).count() > 0

    def check_session_exist(self, session_string):
        return self.session.query(self.db_session).filter(
            self.db_session.session == session_string,
            self.db_session.available > datetime.datetime.today()
        ).count() > 0

    def generate_session_string(self, ip, result, expire_days=7):
        session_string = Security.generate_salt()
        salt = Security.generate_salt()
        day_limit = datetime.datetime.today() + datetime.timedelta(days=expire_days)
        self.session.add(
            self.db_session(session=session_string, salt=salt, available=day_limit, ip=ip, result=result))
        self.session.query(self.db_session).filter(
            self.db_session.available < (datetime.datetime.today() - datetime.timedelta(days=(expire_days+3)))).delete()
        self.session.commit()
        return {"session": session_string, "available": day_limit.strftime("%Y-%m-%d %H:%M:%S"), "salt": salt}

    def get_single_session(self, session_string):
        return self.row2dict(
            self.session.query(self.db_session).filter(self.db_session.session == session_string).first())

    def set_session_authorized(self, session_string, result, username=""):
        obj = self.session.query(self.db_session).filter(self.db_session.session == session_string).all()
        for i in obj:
            i.result = result
            i.username = username
        self.session.commit()
        time.sleep(0.5)

    ### index

    def get_titles_from_pdffile_by_genre(self, genre_string):
        return self.rows2dict(
            self.session.query(self.db_pdffile).filter(self.db_pdffile.genre.like('%' + genre_string + '%'))
                .group_by(self.db_pdffile.title_id).all())

    def get_titles_from_pdffile_by_titleid(self, title_id_str):
        return self.rows2dict(
            self.session.query(self.db_pdffile).filter(self.db_pdffile.title_id == title_id_str).group_by(
                self.db_pdffile.title).all())

    def get_title_changed(self, id_str):
        return self.row2dict(self.session.query(self.db_pdffile).filter(self.db_pdffile.id == id_str).one())["changed"]

    def get_title_changed_by_title_group(self, title_group_str, part=0):
        return \
            self.row2dict(self.session.query(self.db_pdffile).filter(self.db_pdffile.title == title_group_str, self.db_pdffile.part == part).one())[
                "changed"]

    def set_titles_changed(self, id_str, changed=0):
        obj = self.session.query(self.db_pdffile).filter(self.db_pdffile.id == id_str).all()
        for i in obj:
            i.changed = changed
        self.session.commit()

    ### index user pdftitle

    def get_titles_for_user_from_pdffile_user_by_genre(self, username, genre_string=""):
        obj = None
        if len(genre_string) == 0:
            obj = self.rows2dict(
                self.session.query(self.db_pdffile).group_by(self.db_pdffile.title_id).all())
        else:
            obj = self.rows2dict(
                self.session.query(self.db_pdffile).filter(self.db_pdffile.genre.like('%' + genre_string + '%'))
                    .group_by(self.db_pdffile.title_id).all())
        title_id_list = []
        for i in obj:
            title_id_list.append(i["title_id"])
        title_user_list = self.rows2dict(self.session.query(
            self.db_pdffile_user).filter(self.db_pdffile_user.username == username,
                                         self.db_pdffile_user.title_id.in_(title_id_list))
                                         .order_by(self.db_pdffile_user.last.desc()).limit(12).all())
        title_id_list = []
        title_user_list_title_id = {}
        for i in title_user_list:
            title_id_list.append(i["title_id"])
            title_user_list_title_id[i["title_id"]] = i
        obj = self.rows2dict(self.session.query(
            self.db_pdffile).filter(self.db_pdffile.title_id.in_(title_id_list)).group_by(self.db_pdffile.title_id).limit(12).all())
        ret = []
        for i in obj:
            rettmp = i
            tmp = title_user_list_title_id[i["title_id"]]
            rettmp["nowpage"] = tmp["page"]
            rettmp["count"] = tmp["count"]
            rettmp["last"] = tmp["last"]
            ret.append(rettmp)
        return ret

    def get_titles_for_user_from_pdffile_user(self, username):
        return self.get_titles_for_user_from_pdffile_user_by_genre(username, "")

    def add_count_from_pdffile_user(self, username_str, title_id_str, page_int):
        obj = self.session.query(self.db_pdffile_user).filter(self.db_pdffile_user.username == username_str,
                                                              self.db_pdffile_user.title_id == title_id_str).all()
        if len(obj) == 0:
            self.session.add(
                self.db_pdffile_user(username=username_str, page=page_int, title_id=title_id_str, count=1,
                                     last=datetime.datetime.now()))
        else:
            for i in obj:
                i.count = i.count + 1
                i.last = datetime.datetime.today()
                i.page = page_int
        self.session.commit()

    ### update

    def set_pdffile_changed(self):
        obj = self.session.query(self.db_pdffile).all()
        for i in obj:
            i.changed = 1
        self.session.commit()

    ####manager
    def auth_user(self, username):
        obj = self.session.query(self.db_user).filter(self.db_user.username == username).all()
        for i in obj:
            i.auth = 1
        self.session.commit()

    def del_user(self, username):
        self.session.query(self.db_user).filter(self.db_user.username == username).delete()
        self.session.commit()

    def add_user(self, username, sugar, password, auth):
        self.session.add(self.db_user(username=username, sugar=sugar, password=password, auth=auth))
        self.session.commit()

    def get_user_by_username(self, username):
        return self.rows2dict(
            self.session.query(self.db_user).filter(self.db_user.username == username).all()
        )

    def get_user_sort_by_id(self):
        rets_tmp = self.rows2dict(self.session.query(self.db_user).order_by(self.db_user.id).all())
        rets = []
        for ret in rets_tmp:
            t_ret = ret
            t_ret['password'] = ""
            t_ret['sugar'] = ""
            rets.append(t_ret)
        return rets

    def get_genre_sort_by_genre(self):
        return self.rows2dict(
            self.session.query(self.db_genre).order_by(self.db_genre.genre).all()
        )

    def get_session_sort_by_id(self):
        return self.rows2dict(
            self.session.query(self.db_session).order_by(self.db_session.id).all()
        )

    def get_pdffile_sort_by_title(self):
        return self.rows2dict(
            self.session.query(self.db_pdffile).order_by(self.db_pdffile.title).order_by(self.db_pdffile.part).all())

    def get_pdffile_user_sort_by_title(self):
        return self.rows2dict(
            self.session.query(self.db_pdffile_user).order_by(self.db_pdffile_user.last.desc()).all())

    def get_pdffile_by_title_id(self, title_id_str):
        return self.rows2dict(
            self.session.query(self.db_pdffile).filter(self.db_pdffile.title_id == title_id_str).all())

    def set_pdffile_description(self, title_id, spd, index_page):
        obj = self.session.query(self.db_pdffile).filter(self.db_pdffile.title_id == title_id).all()
        for i in obj:
            i.showpagediff = spd
            i.indexpage = index_page
        self.session.commit()

    def delete_pdffile_by_title_id(self, title_id):
        self.session.query(self.db_pdffile).filter(self.db_pdffile.title_id == title_id).delete()
        self.session.commit()

    def delete_genre_by_title_id(self, title_id):
        self.session.query(self.db_genre).filter(self.db_genre.title_id == title_id).delete()
        self.session.commit()

    def insert_genre(self, genre_string, title_id, title_string):
        self.session.add(self.db_genre(genre=genre_string, title=title_string, title_id=title_id))
        self.session.commit()

    def update_genre(self, genre_string, title_id):
        obj = self.session.query(self.db_pdffile).filter(self.db_pdffile.title_id == title_id).all()
        for i in obj:
            i.genre = genre_string
        self.session.commit()

    def add_genre_text(self, title_id, genre_text):
        obj = self.session.query(self.db_pdffile).filter(self.db_pdffile.title_id == title_id).all()
        for i in obj:
            i.genre = genre_text
        self.session.query(self.db_genre).filter(self.db_genre.title_id == title_id).delete()
        for genre_single_string in genre_text.split(","):
            rows = self.rows2dict(
                self.session.query(self.db_pdffile).filter(self.db_pdffile.title_id == title_id).all())
            for row in rows:
                self.session.add(self.db_genre(genre=genre_single_string, title=row['title'], title_id=title_id))
        self.session.commit()

    def add_keys(self, private_key, public_key):
        date = (datetime.datetime.today() + datetime.timedelta(hours=1))
        self.session.add(self.db_key(private=private_key, public=public_key, available=date))

    def get_keys(self):
        return self.rows2dict(self.session.query(self.db_key).filter(
            self.db_key.available > datetime.datetime.today()).all())

    # view
    def search_pdffile_by_title_pagenum(self, title_group, pagenum):
        return self.row2dict(
            self.session.query(self.db_pdffile).filter(
                self.db_pdffile.title_id == title_group,
                self.db_pdffile.startpage < pagenum,
                self.db_pdffile.endpage > pagenum
            ).first())

    def find_pdffile_by_page(self, pdf_id):
        return self.row2dict(
            self.session.query(self.db_pdffile).filter(
                self.db_pdffile.id == pdf_id
            ).first())

    def get_genre_group_by_genre(self):
        return self.rows2dict(self.session.query(self.db_genre).group_by(self.db_genre.genre).all())

    def get_status(self):
        if self.session.query(self.db_status).count() == 0:
            self.session.add(self.db_status())
            self.session.commit()
        return self.row2dict(self.session.query(self.db_status).one())

    def set_status_solr_version(self):
        objs = self.session.query(self.db_status).all()
        for obj in objs:
            obj.solr_version = self.solr_version
        self.session.commit()

    def add_status(self):
        self.session.add(self.db_status())
        self.session.commit()

    def check_latest(self):
        if self.solr_version == self.get_status()['solr_version']:
            return 1
        else:
            return 0

    ##thumbs
    def get_titles_from_genre(self, genre):
        return self.row2dict(
            self.session.query(self.db_pdffile).filter(
                self.db_pdffile.genre == genre
            ).all()
        )

    ##indexer
    def rebuild_index(self):
        rows = self.rows2dict(self.session.query(self.db_pdffile).all())
        for row in rows:
            bpage = 0
            if row['part'] > 0:
                rows2 = self.rows2dict(self.session.query(self.db_pdffile).filter(
                    self.db_pdffile.title == row['title'],
                    self.db_pdffile.part < row['part']
                ).all())
                for row2 in rows2:
                    bpage += row2['page']
            obj = self.session.query(self.db_pdffile).filter(
                self.db_pdffile.title == row['title'],
                self.db_pdffile.part == row['part'],
                self.db_pdffile.id == row['id']
            ).all()
            for i in obj:
                i.startpage = bpage
                i.endpage = bpage + row['page']
        self.session.commit()

    def insert_pdffile(self, id_str, pdf_path, page, title_group, title_id, part_string):
        self.session.add(
            self.db_pdffile(id=id_str, path=pdf_path, page=page, title=title_group, title_id=title_id,
                            part=part_string))
        self.session.commit()

    def change_path_pdffile_by_title_group(self, title_group, pdf_path, id_str, part):
        obj = self.session.query(self.db_pdffile).filter(self.db_pdffile.title == title_group, self.db_pdffile.part == part).all()
        for i in obj:
            i.path = pdf_path
            i.id = id_str
        self.session.commit()

    def check_pdffile_exists(self, pdf_id):
        return self.session.query(self.db_pdffile).filter(
            self.db_pdffile.id == pdf_id
        ).count() > 0

    def check_pdffile_exists_by_title_group(self, title_group, part=0):
        return self.session.query(self.db_pdffile).filter(
            self.db_pdffile.title == title_group,
            self.db_pdffile.part == part
        ).count() > 0

    def import_old_db(self, old_db_path):
        old_db_con = sqlite3.connect(old_db_path, isolation_level=None)
        old_db_con.row_factory = self.dict_factory
        self.__import_old_db_pdffile(old_db_con)
        self.__import_old_db_genre(old_db_con)
        self.__import_old_db_user(old_db_con)
        self.__import_old_db_status(old_db_con)
        old_db_con.close()

    def __import_old_db_pdffile(self, old_db):
        cur_check = old_db.execute("SELECT COUNT(*) FROM sqlite_master WHERE TYPE='table' AND name=?", ("pdffile",))
        if int(cur_check.fetchone()["COUNT(*)"]) == 1:
            old_cur = old_db.execute("SELECT * FROM pdffile")
            old_lists = old_cur.fetchall()
            for i in old_lists:
                if self.session.query(self.db_pdffile).filter(self.db_pdffile.title_id == i['title_id']).count() > 0:
                    obj = self.session.query(self.db_pdffile).filter(self.db_pdffile.title_id == i['title_id']).all()
                    for j in obj:
                        j.path = i['path']
                        j.genre = i['genre']
                        j.page = i['page']
                        j.title = i['title']
                        j.title_id = i['title_id']
                        j.part = i['part']
                        j.startpage = i['startpage']
                        j.endpage = i['endpage']
                        j.showpagediff = i['showpagediff']
                        j.indexpage = i['indexpage']
                        j.changed = 0
                else:
                    self.session.add(
                        self.db_pdffile(id=i['id'], path=i['path'], genre=i['genre'], page=i['page'], title=i['title'],
                                        title_id=i['title_id'], part=i['part'], startpage=i['startpage'],
                                        endpage=i['endpage'], showpagediff=i['showpagediff'], indexpage=i['indexpage'],
                                        changed=0)
                    )
            self.session.commit()

    def __import_old_db_genre(self, old_db):
        cur_check = old_db.execute("SELECT COUNT(*) FROM sqlite_master WHERE TYPE='table' AND name=?", ("genre",))
        if int(cur_check.fetchone()["COUNT(*)"]) == 1:
            old_cur = old_db.execute("SELECT * FROM genre")
            old_lists = old_cur.fetchall()
            for i in old_lists:
                self.session.add(self.db_genre(genre=i['genre'], title=i['title'], title_id=i['title_id']))
        self.session.commit()

    def __import_old_db_user(self, old_db):
        cur_check = old_db.execute("SELECT COUNT(*) FROM sqlite_master WHERE TYPE='table' AND name=?", ("user",))
        if int(cur_check.fetchone()["COUNT(*)"]) == 1:
            old_cur = old_db.execute("SELECT * FROM user")
            old_lists = old_cur.fetchall()
            for i in old_lists:
                self.session.add(
                    self.db_user(username=i['username'], sugar=i['sugar'], password=i['password'],
                                 lastlogin=i['lastlogin'], auth=int(i['auth'])))
        self.session.commit()

    def __import_old_db_status(self, old_db):
        cur_check = old_db.execute("SELECT COUNT(*) FROM sqlite_master WHERE TYPE='table' AND name=?", ("status",))
        if int(cur_check.fetchone()["COUNT(*)"]) == 1:
            old_cur = old_db.execute("SELECT * FROM status")
            old_list = old_cur.fetchone()
            if old_list:
                self.session.add(
                    self.db_status(solr_version=old_list["solr_version"])
                )
                print("old-db solr version:")
                print(old_list["solr_version"])
            else:
                self.session.add(self.db_status(solr_version="0", is_indexed=0))
        else:
            print("solr_version not found")
            self.session.add(self.db_status(solr_version="0", is_indexed=0))
        self.session.commit()

    def get_db_json(self):
        rets = {}
        rets['pdffile'] = self.rows2dict(self.session.query(self.db_pdffile).all())
        rets['genre'] = self.rows2dict(self.session.query(self.db_genre).all())
        rets['status'] = self.rows2dict(self.session.query(self.db_status).all())
        return rets

    def set_db_from_json(self, data):
        libre10_version = data['status'][0]["libre10_version"]
        if libre10_version == "1.3.0" or libre10_version == "1.5.0":
            for i in data['pdffile']:
                if self.session.query(self.db_pdffile).filter(self.db_pdffile.title_id == i['title_id']).count() > 0:
                    obj = self.session.query(self.db_pdffile).filter(self.db_pdffile.title_id == i['title_id']).all()
                    for j in obj:
                        j.path = i['path']
                        j.genre = i['genre']
                        j.page = i['page']
                        j.title = i['title']
                        j.title_id = i['title_id']
                        j.part = i['part']
                        j.startpage = i['startpage']
                        j.endpage = i['endpage']
                        j.showpagediff = i['showpagediff']
                        j.indexpage = i['indexpage']
                        j.changed = 0
                else:
                    self.session.add(
                        self.db_pdffile(id=i['id'], path=i['path'], genre=i['genre'], page=i['page'], title=i['title'],
                                        title_id=i['title_id'], part=i['part'], startpage=i['startpage'],
                                        endpage=i['endpage'], showpagediff=i['showpagediff'], indexpage=i['indexpage'],
                                        changed=1)
                    )
            for i in data['genre']:
                self.session.add(self.db_genre(genre=i['genre'], title=i['title'], title_id=i['title_id']))
                self.session.flush()
            self.session.commit()
        if libre10_version == "1.7.0":
            for i in data['pdffile']:
                if self.session.query(self.db_pdffile).filter(self.db_pdffile.title_id == i['title_id']).count() > 0:
                    obj = self.session.query(self.db_pdffile).filter(self.db_pdffile.title_id == i['title_id']).all()
                    for j in obj:
                        j.path = i['path']
                        j.genre = i['genre']
                        j.page = i['page']
                        j.title = i['title']
                        j.title_id = i['title_id']
                        j.part = i['part']
                        j.startpage = i['startpage']
                        j.endpage = i['endpage']
                        j.showpagediff = i['showpagediff']
                        j.indexpage = i['indexpage']
                        j.changed = 0
                else:
                    self.session.add(
                        self.db_pdffile(id=i['id'], path=i['path'], genre=i['genre'], page=i['page'], title=i['title'],
                                        title_id=i['title_id'], part=i['part'], startpage=i['startpage'],
                                        endpage=i['endpage'], showpagediff=i['showpagediff'], indexpage=i['indexpage'],
                                        changed=1)
                    )
            for i in data['genre']:
                self.session.add(self.db_genre(genre=i['genre'], title=i['title'], title_id=i['title_id']))
                self.session.flush()
            for i in data["pdffile_user"]:
                self.session.add(self.db_pdffile_user(username=i['username'],page=i['page'],title_id=i['title_id'],count=i['count'],last=i['last']))
            self.session.commit()


class PdfToJpeg():
    def __init__(self, cache_path):
        self.cachepath = cache_path

    def __get_image_size(self, image_path):
        f = open(image_path.encode('utf-8'))
        ims = Image.open(f)
        ret = ims.size
        f.close()
        return ret

    def generate_resized_jpeg(self, input_path, output_path, width, unsharp=0):
        sizestr = str(width) + 'x' + str(int(width) * 2)
        calc = 1000000
        try:
            if unsharp == 1:
                subprocess.call(
                    ["gm", "convert", "-unsharp", "0", "-quality", "50", "-size", sizestr, "-resize", sizestr + ">",
                     input_path.encode('utf-8'), output_path.encode('utf-8')])
                calc += 32000000
            else:
                subprocess.call(["gm", "convert", "-quality", "60", "-size", sizestr, "-resize", sizestr + ">",
                                 input_path.encode('utf-8'), output_path.encode('utf-8')])
                calc += 2000000
        except OSError:
            subprocess.call(["convert", "-define", "jpeg:size=" + sizestr, "-quality", "60", "-thumbnail",
                             sizestr + ">", input_path.encode('utf-8'), output_path.encode('utf-8')])
            calc += 4000000
        try:
            subprocess.call(["jpegtran", "-copy", "none", "-optimize", "-outfile", output_path.encode('utf-8'),
                             output_path.encode('utf-8')])
            calc += 8000000
        except OSError:
            calc += 16000000

    def get_jpeg_from_pdf(self, pdf_path, pdf_page, output_path):
        try:
            subprocess.call(
                ["pdftoppm", "-f", pdf_page, "-l", pdf_page, "-r", "400", "-jpeg", pdf_path.encode('utf-8'),
                 output_path.encode('utf-8')])
        except OSError:
            return None
        page_string = output_path + "-" + str(pdf_page).zfill(3)
        for i in xrange(0, 7):
            if os.path.exists((output_path + "-" + str(pdf_page).zfill(i) + ".jpg").encode('utf-8')):
                page_string = output_path + "-" + str(pdf_page).zfill(i)
        if not os.path.exists((page_string + ".jpg").encode('utf-8')):
            if os.path.exists((page_string + ".ppm").encode('utf-8')):
                try:
                    subprocess.call(
                        ['cjpeg', (page_string + ".ppm").encode('utf-8'), ">",
                         (page_string + ".jpg").encode('utf-8')])
                except OSError:
                    None
                os.remove((page_string + ".ppm").encode('utf-8'))
            elif os.path.exists((page_string + ".pbm").encode('utf-8')):
                try:
                    subprocess.call(
                        ['convert', (page_string + ".pbm").encode('utf-8'), (page_string + ".jpg").encode('utf-8')])
                except OSError:
                    None
        if os.path.exists((page_string + ".jpg").encode('utf-8')):
            shutil.move((page_string + ".jpg").encode('utf-8'), output_path.encode('utf-8'))
        subprocess.call(["rm", "-f", (output_path + "*.jpg").encode("utf-8")])
        subprocess.call(["rm", "-f", (output_path + "*.ppm").encode('utf-8')])
        subprocess.call(["rm", "-f", (output_path + "*.pbm").encode('utf-8')])

    def get_jpeg_optimized(self, jpeg_path):
        try:
            subprocess.call(['jpegtran', '-copy', 'none', '-optimize', '-outfile', jpeg_path.encode("utf-8"),
                             jpeg_path.encode("utf-8")])
        except OSError:
            None

    def __get_matching_cache_file(self, width, title_string, pdf_page, permit_expand_x=1.1):
        cachepathmobile = '%s/900/%s/%s_%s.jpg' % (self.cachepath, title_string, title_string, pdf_page)
        cache1200 = '%s/1200/%s/%s_%s.jpg' % (self.cachepath, title_string, title_string, pdf_page)
        cache2400 = '%s/2400/%s/%s_%s.jpg' % (self.cachepath, title_string, title_string, pdf_page)
        cachepath = '%s/orig/%s/%s_%s.jpg' % (self.cachepath, title_string, title_string, pdf_page)
        cache_file_list = [cachepathmobile, cache1200, cache2400, cachepath]
        cache_file_exist_list = [
            os.path.exists(cachepathmobile.encode("utf-8")), os.path.exists(cache1200.encode("utf-8")),
            os.path.exists(cache2400.encode("utf-8")), os.path.exists(cachepath.encode("utf-8"))]
        if os.path.exists(cachepath.encode("utf-8")):
            cache_file_size_list = [900, 1200, 2400, int(self.__get_image_size(cachepath)[0])]
            cache_file_min_size_list = [900 * permit_expand_x, 1200 * permit_expand_x, 2400 * permit_expand_x,
                                        int(int(self.__get_image_size(cachepath)[0]) * permit_expand_x)]
        else:
            return {"cache_jpeg_path": "", "cache_width": -1}

        ret = {"cache_jpeg_path": "", "cache_width": -1}
        for i in xrange(0, len(cache_file_list)):
            if cache_file_min_size_list[i] >= int(width):
                for j in xrange(i, len(cache_file_list)):
                    if cache_file_exist_list[j]:
                        ret["cache_jpeg_path"] = cache_file_list[j]
                        ret["cache_width"] = cache_file_size_list[j]
                        return ret
        if cache_file_exist_list[len(cache_file_exist_list) - 1]:
            ret["cache_jpeg_path"] = cache_file_list[len(cache_file_exist_list) - 1]
            ret["cache_width"] = cache_file_size_list[len(cache_file_exist_list) - 1]
        return ret

    def get_jpeg_string_from_pdf(self, pdf_path, pdf_page, width, is_light=0):
        import random
        import string

        self.outputpath = os.path.join(self.cachepath,
                                       ''.join(random.choice(string.digits + string.letters) for i in xrange(8)))
        titlestr = os.path.splitext(os.path.split(pdf_path)[1])[0]
        calc = 0
        allow_width = [{'min': 0.7, 'max': 1.2}, {'min': 0.9, 'max': 1.1}]
        cache_jpegs = self.__get_matching_cache_file(width, titlestr, pdf_page, allow_width[is_light]['max'])
        if cache_jpegs["cache_width"] < 0:
            calc += 1
            if os.access(pdf_path.encode("utf-8"), os.R_OK):
                self.get_jpeg_from_pdf(pdf_path, pdf_page, "%s.jpg" % (self.outputpath,))
                calc += 2
            self.outputpathjpg = "%s.jpg" % (self.outputpath,)
            cache_jpegs = {"cache_width": self.__get_image_size(self.outputpathjpg)[0],
                           "cache_jpeg_path": self.outputpathjpg}
        calc += int(cache_jpegs["cache_width"]) * 10000000
        if int(cache_jpegs["cache_width"]) * allow_width[is_light]['min'] < int(width) or int(
                cache_jpegs["cache_width"]) < int(width):
            self.outputpathjpg = cache_jpegs["cache_jpeg_path"]
            calc += 4
        else:
            outputrspathjpg = "%s-000-%s-%s.jpg" % (self.outputpath, str(width), str(int(width) * 2))
            self.generate_resized_jpeg(cache_jpegs["cache_jpeg_path"], outputrspathjpg, width, unsharp=0)
            self.outputpathjpg = outputrspathjpg
            calc += 8
        calc += width * 100
        if os.path.exists(self.outputpathjpg.encode("utf-8")):
            import io

            wf = io.open(self.outputpathjpg.encode("utf-8"), 'br')
            wfstr = wf.read()
            wf.close()
            subprocess.call(['rm', self.outputpath + "*"])
            if os.path.exists(("%s.jpg" % (self.outputpath,)).encode("utf-8")):
                os.remove(("%s.jpg" % (self.outputpath,)).encode("utf-8"))
            if os.path.exists(("%s-000.jpg" % (self.outputpath,)).encode("utf-8")):
                os.remove(("%s-000.jpg" % (self.outputpath,)).encode("utf-8"))
            if os.path.exists(
                    ("%s-000-%s-%s.jpg" % (self.outputpath, str(width), str(int(width) * 2))).encode("utf-8")):
                os.remove(("%s-000-%s-%s.jpg" % (self.outputpath, str(width), str(int(width) * 2))).encode("utf-8"))
            return [wfstr, calc, cache_jpegs]  # fixme cache_jpegs
        return ["", calc, cache_jpegs]

    def delete_pdf_cache_files(self, pdf_path):
        title_string = os.path.splitext(os.path.split(pdf_path)[1])[0]
        cachepathmobile = '%s/900/%s' % (self.cachepath, title_string)
        cache1200 = '%s/1200/%s' % (self.cachepath, title_string)
        cache2400 = '%s/2400/%s' % (self.cachepath, title_string)
        cachepath = '%s/orig/%s' % (self.cachepath, title_string)
        if len(title_string) > 0:
            if os.path.exists(cachepathmobile.encode('utf-8')):
                shutil.rmtree(cachepathmobile.encode('utf-8'))
            if os.path.exists(cache1200.encode('utf-8')):
                shutil.rmtree(cache1200.encode('utf-8'))
            if os.path.exists(cache2400.encode('utf-8')):
                shutil.rmtree(cache2400.encode('utf-8'))
            if os.path.exists(cachepath.encode('utf-8')):
                shutil.rmtree(cachepath.encode('utf-8'))
        if os.path.exists(pdf_path.encode('utf-8')):
            shutil.move(pdf_path.encode('utf-8'), (pdf_path + ".bak").encode('utf-8'))


class PdfToText():
    def __init__(self, cache_path):
        self.cachepath = cache_path

    def get_page_number_from_pdf(self, pdf_path):
        process = subprocess.Popen(['pdfinfo', pdf_path.encode("utf-8")], stdout=subprocess.PIPE)
        out, err = process.communicate()
        ret = 1
        for st in unicode(out, errors='ignore').split("\n"):
            if st.find("Pages") > -1:
                ret = int(st.replace("Pages", "").replace(":", "").replace(" ", ""))
        return ret

    def get_title_string_from_pdf_path(self, pdf_path):
        import re

        if os.path.basename(pdf_path).find("_Part") > -1:
            title_group = os.path.basename(pdf_path).split("_Part")[0]
            print(title_group)
            parttext = os.path.basename(pdf_path).split("_Part")[1]
            if parttext.find("_") > -1:
                parttext = parttext.split("_")[0]
            print(parttext)
            if parttext.find(".pdf") > -1:
                try:
                    parttext = int(parttext.split(".pdf")[0])
                except:
                    parttext = int(re.split(r"\D", parttext.split(".pdf")[0])[0])
            else:
                parttext = int(parttext)
        else:
            parttext = 0
            title_group = os.path.basename(pdf_path).split(".pdf")[0]
        return {'title': title_group, 'part': parttext}

    def save_text_from_pdf(self, pdf_path, tmp_dest_path, page_number):
        subprocess.call(['pdftotext', '-eol', 'unix', '-enc', 'UTF-8', '-f', str(page_number), '-l', str(page_number),
                         pdf_path.encode("utf-8"), tmp_dest_path.encode("utf-8")])

    def get_text_postion_from_pdf_texts(self, searchtext, pdf_path, page_number, cache_folder):
        lists = self.get_position_texts_from_pdf(pdf_path, page_number, cache_folder)
        lists['pos'] = []
        for i in lists['text']:
            if i['text'].lower().find(searchtext.lower()) > 0:
                lists['pos'].append(i)
        return lists

    def get_position_texts_from_pdf(self, pdf_path, page_number, cache_folder):
        import random

        title_string = os.path.splitext(os.path.split(pdf_path)[1])[0]
        cache_path_text = '%s/text/%s/%s.xml' % (self.cachepath, title_string, page_number)
        if os.path.exists(cache_path_text.encode("utf-8")):
            return self.__get_position_from_xml(cache_path_text)
        else:
            random.seed()
            fn_str = str(random.randint(0, 10000000)) + '.xml'
            self.save_position_text_from_pdf(pdf_path, os.path.join(cache_folder, fn_str), page_number)
            return self.__get_position_from_xml(os.path.join(cache_folder, fn_str))

    def save_position_texts_cache_from_pdf(self, pdf_path, page_number):
        title_string = os.path.splitext(os.path.split(pdf_path)[1])[0]
        cache_path_text = '%s/text/%s/%s.xml' % (self.cachepath, title_string, page_number)
        self.save_position_text_from_pdf(pdf_path, cache_path_text, page_number)

    def __get_position_from_xml(self, xml_path):
        import xml.etree.cElementTree

        rets = {}
        xmltree = xml.etree.cElementTree.parse(xml_path.encode("utf-8"))
        elem = xmltree.getroot()
        page_tag = elem.find("page")
        rets['page'] = {'top': int(page_tag.get('top')), 'left': int(page_tag.get('left')),
                        'width': int(page_tag.get('width')),
                        'height': int(page_tag.get('height'))}
        rets['text'] = []
        for e in elem.findall("page/text"):
            ret = {}
            if e.text:
                ret['text'] = e.text.replace(" ", "").replace("　", "")
            else:
                ret['text'] = ""
            ret['top'] = int(100 * (int(e.get('top')) - rets['page']['top']) / rets['page']['height'])
            ret['left'] = int(100 * (int(e.get('left')) - rets['page']['left']) / rets['page']['width'])
            ret['width'] = int(100 * (int(e.get('width'))) / rets['page']['width'])
            ret['height'] = int(100 * (int(e.get('height'))) / rets['page']['height'])
            rets['text'].append(ret)
        return rets

    def save_position_text_from_pdf(self, pdf_path, dest_path, page_number):
        subprocess.call(
            ['pdftohtml', '-enc', 'UTF-8', '-c', '-i', '-hidden', '-xml', '-f', str(page_number), '-l',
             str(page_number),
             pdf_path.encode("utf-8"), dest_path.encode("utf-8")])


class SolrAccess():
    def __init__(self, db, solr_url):
        self.defaultSolrDic = {
            'rows': '30', 'start': '0', 'wt': 'json',
            'group': 'true', 'group.limit': '20', 'group.field': 'title_group',
            'hl': 'true', 'hl.fl': 'text', 'hl.highlightMultiTerm': 'true', 'hl.snippets': '2',
            'hl.alternateField': 'text', 'hl.maxAnalyzedChars': '51200', 'hl.bs.type': 'SENTENCE',
            'hl.tag.pre': '<span style=background-color:yellow>', 'hl.tag.post': '</span>',
        }
        self.solrurl = solr_url
        self.db = db

    def get_schema_version(self):
        r = requests.get(self.solrurl + "/schema/version")
        return r.json()["version"]

    def search(self, solrdic):
        r = requests.get(self.solrurl + "/select/?", params=solrdic)
        if r.status_code > 400:
            time.sleep(3)
            r = requests.get(self.solrurl + "/select/?", params=solrdic)
        return r.json()

    def search_query(self, query_text, title_group_list_str="", start="0"):
        queryDic = self.defaultSolrDic.copy()
        queryDic['q'] = query_text.encode("utf-8")
        queryDic['start'] = start
        if len(title_group_list_str) > 0:
            queryDic['facet'] = 'true'
            queryDic['fq'] = "title_group:%s" % title_group_list_str
        return self.search(queryDic)

    def solr_response_to_html_group(self, responseDic):
        rets = []
        grouplist = responseDic['grouped']['title_group']['groups']
        hlgroup = responseDic['highlighting']
        for gs in grouplist:
            ret = []
            for textarray in gs['doclist']['docs']:
                pagenum = textarray['page']
                path = textarray['path_id']
                pagedb = self.db.find_pdffile_by_page(path)
                if pagedb:
                    hl_text = hlgroup[textarray['id']]['text'][0]
                    if len(hl_text) > 600:
                        match_index = hl_text.find("backround-color:yellow")
                        if match_index > 300 or match_index > len(hl_text) - 300:
                            hl_text = hl_text[match_index - 300: match_index + 300]
                        elif 300 > match_index > -1:
                            hl_text = hl_text[0:600]
                        elif match_index > len(hl_text) - 300:
                            hl_text = hl_text[len(hl_text) - 600:]
                        else:
                            hl_text = hl_text[len(hl_text) / 2 - 300: len(hl_text) / 2 + 300]
                    showpagenum = pagenum + pagedb['startpage']
                    ret.append({'page': showpagenum, 'hl_text': hl_text,
                                'title': pagedb['title_id']})
            rets.append({'search_result': ret, 'text': gs['doclist']['docs'][0]['title_group'],
                         'search_num': gs['doclist']['numFound']})
        return rets

    def get_document_num(self):
        queryDic = {"q": "*:*", 'wt': 'json'}
        return self.search(queryDic)["response"]["numFound"]

    def lists_dic_to_solr_query_string(self, lists, key):
        return "(\"" + "\" OR \"".join([i[key] for i in lists]) + "\")"

    def delete_title_group(self, title_group):  # pdffileのtitle
        url = self.solrurl + "/update?commit=true"
        data = {"delete": {"query": "title_group:\"%s\"" % title_group}}
        headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
        r = requests.post(url, data=anyjson.serialize(data), headers=headers)
        return r.json()

    ##indexer
    def __add_data(self, data):
        url = self.solrurl + "/update"
        headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
        r = requests.post(url, data=anyjson.serialize(data), headers=headers)
        if r.status_code > 400:
            time.sleep(5)
            requests.post(url, data=anyjson.serialize(data), headers=headers)

    def delete_all(self):
        url = self.solrurl + "/update?commit=true&stream.body=<delete><query>*:*</query></delete>"
        requests.get(url)

    def commit(self):
        url = self.solrurl + "/update?commit=true"
        requests.get(url)

    def send_string_to_solr(self, id_string, page_number, page_max_number, title_string, title_group_string,
                            title_group_id_string,
                            data_string, path_id_string):
        sdata = [{
            'id': id_string + "_" + str(page_number), 'title': title_string, 'title_group': title_group_string,
            'title_group_id': title_group_id_string, 'page': int(page_number), 'pagemax': int(page_max_number),
            'text': title_string + data_string, 'path_id': path_id_string
        }]
        self.__add_data(sdata)

    def send_text_file_to_solr(self, title_string, txt_file_path, page_number, page_max_number, id_number):
        import codecs
        import re

        rex1 = re.compile(ur'[\n<>&\x0c]')
        rex2 = re.compile(ur'([^0-9a-zA-Z.,_\-])[/s ]+?([^0-9a-zA-Z.,_\-])')
        rex3 = re.compile('\x00')
        f = codecs.open(txt_file_path, "r", "utf-8", "ignore")
        data1 = f.read()
        f.close()
        data1 = rex1.sub('', data1)
        data1 = rex2.sub(r'\1\2', data1)
        data1 = rex3.sub('', data1)
        data1 = rex2.sub(r'\1\2', data1)
        data1 = rex3.sub('', data1)
        title_group_string = title_string.split("_Part")[0]
        title_group_id_string = hashlib.sha224(title_group_string.encode("utf-8")).hexdigest()
        self.send_string_to_solr(id_number, page_number, page_max_number, title_string, title_group_string,
                                 title_group_id_string, data1, id_number)

    def optimize(self):
        url = self.solrurl + "/update?optimize=true"
        requests.get(url)


class Security():
    @staticmethod
    def generate_random_int():
        import sys
        from Crypto.Random import random

        return random.randint(1, sys.maxint - 1)

    @staticmethod
    def generate_password_hash(salt, pass_string):
        return hashlib.sha256(pass_string + salt).hexdigest()

    @staticmethod
    def generate_salt():
        return hashlib.sha256(str(Security.generate_random_int())).hexdigest()

    @staticmethod
    def generate_password_with_salt_hash(password_hash, salt):
        return hashlib.sha256(password_hash + salt).hexdigest()

