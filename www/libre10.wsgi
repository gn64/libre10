#!/usr/bin/python
# -*- coding:utf-8 -*-

import sys
import os

# Libre10 web based pdf search engine
# Copyright (C) 2013-15 yukikaze

dirpath = os.path.dirname(os.path.abspath(__file__))
sys.path.append(dirpath)
os.chdir(dirpath)

import bottle
import libre10_wsgi

application = bottle.default_app()