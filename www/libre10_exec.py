#!/usr/bin/python
# -*- coding:utf-8 -*-

"""Libre10

Libre10 web based pdf search engine
Copyright (C) 2013-2014 yukikaze

Usage:
    libre10 import [--force] [-f]
    libre10 index
    libre10 install [--data-dir=<data_dir>] [--bin-dir=<bin_dir>] [--www-dir=<www_dir>] [--dest-dir=<dest_dir>] [--disable-jetty-bin] [--disable-wsgi-bin] [--enable-systemd-service] [--python-path=<python_path>] [--disable-env]
    libre10 remove [--data-dir=<data_dir>] [--bin-dir=<bin_dir>] [--www-dir=<www_dir>]
    libre10 update
    libre10 -h | --help
    libre10 --version
Options:
    -h --help   Show this screen.
    --data-dir=<data_dir>   Data dir [default: /opt]
    --bin-dir=<bin_dir>     Bin dir [default: /usr/local/bin]
    --www-dir=<www_dir>     WWW dir [default: /var/www]
    --python-path=<python_path> PYTHON PATH [default: ]
    --dest-dir=<dest_dir>   DEST dir [default: ]
    --disable-jetty-bin      Do not install exec script of jetty (solr using port 8993)
    --disable-wsgi-bin       Do not install exec script of wsgi (libre10 search cgi using port 8008)
    --enable-systemd-service    Install systemctl related files.
    --force                 Overwrite resized jpeg files even if cahce jpeg file exist.
"""

from __future__ import print_function, unicode_literals
import os
import os.path
import hashlib
import subprocess
import datetime

from docopt import docopt
import libre10

# Libre10 web based pdf search engine
# Copyright (C) 2013-2015 yukikaze
#

def all_copy(src, dest):
    import errno

    try:
        shutil.copytree(src, dest)
    except OSError as e:
        if e.errno == errno.ENOTDIR:
            shutil.copy(src, dest)
        else:
            print('Directory not copied. Error: %s' % e)


def send_text_from_pdf_to_solr(pdf_path, dest_path, id_number, conf):
    sqlpath = conf.get("path", "dburl")
    pdf_to_text = libre10.PdfToText(conf.get("cache", "cacheFolder"))
    page_number = pdf_to_text.get_page_number_from_pdf(pdf_path)
    tget = pdf_to_text.get_title_string_from_pdf_path(pdf_path)
    title_group = tget['title']
    part_string = tget['part']
    rdl = hashlib.sha224(title_group.encode("utf-8")).hexdigest()
    with libre10.Libre10db(sqlpath) as db:
        solr_access = libre10.SolrAccess(db, conf.get("path", "solrurl"))
        print(pdf_path.encode("utf-8"))
        print(title_group.encode("utf-8"))
        print("Part")
        print(part_string)
        for i in xrange(1, page_number + 1):
            print("page %s / %s" % (str(i), str(page_number + 1)))
            temp_dest_path = "%s/page-%s%s" % (conf.get("cache", 'cacheFolder'), str(i), dest_path)
            pdf_to_text.save_text_from_pdf(pdf_path, temp_dest_path, i)
            cache_path_text = '%s/text/%s/%s.xml' % (
                conf.get("cache", "cacheFolder"), os.path.splitext(os.path.split(pdf_path)[1])[0], page_number)
            pdf_to_text.save_position_texts_cache_from_pdf(pdf_path, i)
            try:
                os.chmod(cache_path_text, 0777)
            except:
                ""
            solr_access.send_text_file_to_solr(os.path.splitext(os.path.basename(pdf_path))[0], temp_dest_path, i,
                                               page_number, id_number)
            os.remove(temp_dest_path)
        solr_access.commit()
        if db.check_pdffile_exists(id_number):
            if not db.get_title_changed(id_number) == 1:
                db.insert_pdffile(id_number, pdf_path, page_number, title_group, rdl, part_string)
        else:
            db.insert_pdffile(id_number, pdf_path, page_number, title_group, rdl, part_string)


def create_caches(pdf_path, conf, forced=0):
    pdf_to_text = libre10.PdfToText(conf.get("cache", "cacheFolder"))
    page_number = pdf_to_text.get_page_number_from_pdf(pdf_path)
    for i in xrange(1, page_number + 1):
        print("image_cache page %s / %s" % (str(i), str(page_number + 1)))
        generate_cache(pdf_path, os.path.splitext(os.path.basename(pdf_path))[0] + "_" + str(i), str(i), conf, forced)


def fileProcess_text(filename, conf, forced=0):
    ext = os.path.splitext(filename)
    print(ext)
    with libre10.Libre10db(conf.get("path", "dburl")) as db:
        if ext[1].lower() == ".pdf":
            filepath = os.path.abspath(filename)
            rd = hashlib.md5()
            rd.update(filepath.encode("utf-8"))
            rdl = rd.hexdigest()
            check_cache_folder(os.path.splitext(os.path.basename(filepath))[0], conf.get("cache", "cacheFolder"))
            if db.check_pdffile_exists(rdl):
                if db.get_title_changed(rdl) == 1:
                    send_text_from_pdf_to_solr(filepath, str(rdl) + ".txt", rdl, conf)
                    db.rebuild_index()
                    db.set_titles_changed(rdl, changed=2)
            else:
                pdf_to_text = libre10.PdfToText(conf.get("cache", "cacheFolder"))
                tget = pdf_to_text.get_title_string_from_pdf_path(filepath)
                title_group = tget['title']
                if db.check_pdffile_exists_by_title_group(title_group, tget['part']):
                    db.change_path_pdffile_by_title_group(title_group, filepath, rdl, tget['part'])
                    if db.get_title_changed_by_title_group(title_group, tget['part']) == 1:
                        send_text_from_pdf_to_solr(filepath, str(rdl) + ".txt", rdl, conf)
                        db.rebuild_index()
                    db.set_titles_changed(rdl, changed=2)
                else:
                    print(filepath.encode("utf-8"))
                    send_text_from_pdf_to_solr(filepath, str(rdl) + ".txt", rdl, conf)
                    db.rebuild_index()
                    db.set_titles_changed(rdl, changed=2)


def fileProcess_image(filename, conf, forced=0):
    ext = os.path.splitext(filename)
    print(ext)
    with libre10.Libre10db(conf.get("path", "dburl")) as db:
        if ext[1].lower() == ".pdf":
            filepath = os.path.abspath(filename)
            rd = hashlib.md5()
            rd.update(filepath.encode("utf-8"))
            rdl = rd.hexdigest()
            if db.check_pdffile_exists(rdl):
                if db.get_title_changed(rdl) == 2:
                    create_caches(filepath, conf, forced)
                    db.set_titles_changed(rdl, changed=0)
            else:
                pdf_to_text = libre10.PdfToText(conf.get("cache", "cacheFolder"))
                tget = pdf_to_text.get_title_string_from_pdf_path(filepath)
                title_group = tget['title']
                if db.check_pdffile_exists_by_title_group(title_group, tget['part']):
                    if db.get_title_changed_by_title_group(title_group, tget['part']) == 2:
                        create_caches(filepath, conf, forced)
                    db.set_titles_changed(rdl, changed=0)
                else:
                    print(filepath.encode("utf-8"))
                    db.set_titles_changed(rdl, changed=0)


def dirProcess(dirname, conf, forced=0):
    list = os.listdir(dirname.encode("utf-8"))
    print("text indexing...")
    for fn in list:
        print(fn)
        fileProcess_text(unicode(os.path.join(dirname, fn), "utf-8"), conf)
    print("creating jpeg caches...")
    for fn in list:
        print(fn)
        fileProcess_image(unicode(os.path.join(dirname, fn), "utf-8"), conf, forced)


def check_cache_folder(folder_name, cacheFolder):
    if not os.path.exists(os.path.join(os.path.join(cacheFolder, "orig"), folder_name).encode("utf-8")):
        try:
            os.mkdir(os.path.join(os.path.join(cacheFolder, "orig"), folder_name).encode("utf-8"))
            os.chmod(os.path.join(os.path.join(cacheFolder, "orig"), folder_name).encode("utf-8"), 0777)
        except:
            ""
    if not os.path.exists(os.path.join(os.path.join(cacheFolder, "1200"), folder_name).encode("utf-8")):
        try:
            os.mkdir(os.path.join(os.path.join(cacheFolder, "1200"), folder_name).encode("utf-8"))
            os.chmod(os.path.join(os.path.join(cacheFolder, "1200"), folder_name).encode("utf-8"), 0777)
        except:
            ""
    if not os.path.exists(os.path.join(os.path.join(cacheFolder, "2400"), folder_name).encode("utf-8")):
        try:
            os.mkdir(os.path.join(os.path.join(cacheFolder, "2400"), folder_name).encode("utf-8"))
            os.chmod(os.path.join(os.path.join(cacheFolder, "2400"), folder_name).encode("utf-8"), 0777)
        except:
            ""
    if not os.path.exists(os.path.join(os.path.join(cacheFolder, "text"), folder_name).encode("utf-8")):
        try:
            os.mkdir(os.path.join(os.path.join(cacheFolder, "text"), folder_name).encode("utf-8"))
            os.chmod(os.path.join(os.path.join(cacheFolder, "text"), folder_name).encode("utf-8"), 0777)
        except:
            ""


def generate_cache(pdf_path, filename, page_number_string, conf, forced=0):
    cacheFolder = conf.get("cache", 'cacheFolder')
    pdf_jpeg = libre10.PdfToJpeg(cacheFolder)
    folder_name = os.path.splitext(os.path.basename(pdf_path))[0]
    cacheJpegPath = os.path.join(os.path.join(os.path.join(cacheFolder, "orig"), folder_name), filename + ".jpg")
    check_cache_folder(folder_name, cacheFolder)
    if conf.getboolean("cache", "extendJpeg") == 1:
        checkPathExists = os.path.exists(cacheJpegPath)
        if checkPathExists:
            checkPathExists = os.path.getsize(cacheJpegPath)>0
        if not checkPathExists or forced==1:
            pdf_jpeg.get_jpeg_from_pdf(pdf_path, page_number_string, cacheJpegPath)
            pdf_jpeg.get_jpeg_optimized(cacheJpegPath)
            try:
                os.chmod(cacheJpegPath, 0777)
            except:
                ""
            if conf.getboolean("cache", "cache_width_1200") == 1:
                cacheMobileJpegPath = "%s/1200/%s/%s.jpg" % (cacheFolder, folder_name, filename)
                pdf_jpeg.generate_resized_jpeg(cacheJpegPath, cacheMobileJpegPath, 1200, unsharp=1)
                pdf_jpeg.get_jpeg_optimized(cacheMobileJpegPath)
            if conf.getboolean("cache", "cache_width_2400") == 1:
                cacheMobileJpegPath = "%s/2400/%s/%s.jpg" % (cacheFolder, folder_name, filename)
                pdf_jpeg.generate_resized_jpeg(cacheJpegPath, cacheMobileJpegPath, 2400, unsharp=1)
                pdf_jpeg.get_jpeg_optimized(cacheMobileJpegPath)


if __name__ == '__main__':
    arg_list = docopt(__doc__, version="1.8.0")  # fixme version
    print(arg_list)
    if arg_list['import'] or arg_list['index']:
        conf = libre10.Config()
        sqlpath = conf.get("path", "dburl")
        if not os.path.exists(os.path.join(conf.get("cache", "cacheFolder"), "text")):
            try:
                os.mkdir(os.path.join(conf.get("cache", "cacheFolder"), "text"))
                os.chmod(os.path.join(conf.get("cache", "cacheFolder"), "text"), 0777)
            except:
                ""
        forced = arg_list['--force'] or arg_list['-f']
        os.system("chmod 777 " + sqlpath)
        dirProcess(conf.get("path", "pdfpath"), conf, forced)
        subprocess.call(["chmod", "-R", "777", conf.get("path", "pdfpath")])
        dirProcess(conf.get("path", "uploadpath"), conf, forced)
        subprocess.call(["chmod", "-R", "777", conf.get("path", "uploadpath")])
        with libre10.Libre10db(sqlpath) as db:
            print("Solr database optimize start.")
            solr_access = libre10.SolrAccess(db, conf.get("path", "solrurl"))
            solr_access.optimize()
            db.set_status_solr_version()
            print("Solr database optimze finished.")
            os.chmod(sqlpath, 0777)
    elif arg_list['update']:
        import shutil

        conf = libre10.Config()
        sqlpath = conf.get("path", "dburl")
        if not os.path.exists(sqlpath):
            subprocess.call(["touch", sqlpath])
            with libre10.Libre10db(sqlpath) as db:
                print("Database not found, create new one.")
                db.add_status()
                os.chmod(sqlpath, 0777)
        else:
            dstr = datetime.datetime.today().strftime("%Y-%m-%d %H:%M:%S")
            old_db_path = sqlpath + dstr + ".bak"
            shutil.move(sqlpath, old_db_path)
            with libre10.Libre10db(sqlpath) as db:
                print("Updating database...")
                db.import_old_db(old_db_path)
                shutil.copystat(old_db_path, sqlpath)
                os.chmod(sqlpath, 0777)
                print("Updating database finished")

    elif arg_list['install']:
        import shutil

        data_dir = arg_list['--data-dir']
        bin_dir = arg_list['--bin-dir']
        www_dir = arg_list['--www-dir']
        systemd_dir = "/etc/systemd/system"
        libre10_dir = os.path.join(data_dir, "libre10")
        dest_dir = arg_list['--dest-dir']
        dest_data_dir = data_dir
        dest_bin_dir = bin_dir
        dest_www_dir = www_dir
        dest_libre10_dir = libre10_dir
        dest_systemd_dir = systemd_dir
        if len(dest_dir) > 0:
            dest_bin_dir = os.path.join(dest_dir, "." + bin_dir)
            dest_data_dir = os.path.join(dest_dir, "." + data_dir)
            dest_www_dir = os.path.join(dest_dir, "." + www_dir)
            dest_libre10_dir = os.path.join(dest_dir, "." + libre10_dir)
            dest_systemd_dir = os.path.join(dest_dir, "." + systemd_dir)
        top_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        subprocess.call(["mkdir", "-p", dest_libre10_dir])
        subprocess.call(["mkdir", "-p", dest_bin_dir])
        subprocess.call(["mkdir", "-p", dest_www_dir])
        subprocess.call(["mkdir", "-p", dest_systemd_dir])
        subprocess.call(["mkdir", "-p", os.path.join(dest_libre10_dir, "db")])
        subprocess.call(["mkdir", "-p", os.path.join(dest_libre10_dir, "pdf")])
        subprocess.call(["mkdir", "-p", os.path.join(dest_libre10_dir, "pdf-upload")])
        subprocess.call(["mkdir", "-p", os.path.join(dest_libre10_dir, "cache")])
        subprocess.call(["mkdir", "-p", os.path.join(os.path.join(dest_libre10_dir, "cache"), "2400")])
        subprocess.call(["mkdir", "-p", os.path.join(os.path.join(dest_libre10_dir, "cache"), "1200")])
        subprocess.call(["mkdir", "-p", os.path.join(os.path.join(dest_libre10_dir, "cache"), "orig")])
        subprocess.call(["mkdir", "-p", os.path.join(os.path.join(dest_libre10_dir, "cache"), "text")])
        subprocess.call(["mkdir", "-p", os.path.join(dest_www_dir, "libre10")])
        # TODO: update solr version.
        subprocess.call(["cp", "-R", os.path.join(top_dir, "solr-5.1.0/"), os.path.join(dest_libre10_dir, "solr")])
        subprocess.call(
            ["cp", "-R", os.path.join(top_dir, "solr/libre10"), os.path.join(dest_libre10_dir, "solr/server/solr")])
        if os.path.exists(os.path.join(os.path.join(dest_libre10_dir, "db"), "libre10.db")):
            last_modified_str = datetime.datetime.fromtimestamp(
                os.stat(os.path.join(os.path.join(top_dir, "conf"), "libre10.db")).st_mtime).strftime("%Y-%m-%d")
            subprocess.call(
                ["cp", os.path.join(os.path.join(dest_libre10_dir, "db"), "libre10.db"),
                 os.path.join(os.path.join(dest_libre10_dir, "db"), "libre10.db.") + last_modified_str])
        else:
            subprocess.call(
                ["cp", os.path.join(os.path.join(top_dir, "conf"), "libre10.db"), os.path.join(dest_libre10_dir, "db")])
        for f_name in os.listdir(os.path.join(top_dir, "www")):
            if os.path.isfile(os.path.join(os.path.join(top_dir, "www"), f_name)):
                shutil.copy(os.path.join(os.path.join(top_dir, "www"), f_name),
                            os.path.join(os.path.join(dest_www_dir, "libre10"), f_name))

        all_copy(os.path.join(os.path.join(os.path.join(top_dir, "doc"), "build"), "html") + "/",
                 os.path.join(os.path.join(dest_www_dir, "libre10"), "help"))
        all_copy(os.path.join(os.path.join(top_dir, "www"), "images") + "/",
                 os.path.join(os.path.join(dest_www_dir, "libre10"), "images"))
        all_copy(os.path.join(os.path.join(top_dir, "www"), "views") + "/",
                 os.path.join(os.path.join(dest_www_dir, "libre10"), "views"))
        # subprocess.call(["cp", "-r", os.path.join(os.path.join(os.path.join(top_dir, "doc"), "build"), "html") + "/",
        # os.path.join(os.path.join(dest_www_dir, "libre10"), "help")])
        new_conf = libre10.Config(os.path.join(os.path.join(top_dir, "conf"), "libre10.conf"))
        new_conf.set("path", "dburl", os.path.join(os.path.join(libre10_dir, "db"), "libre10.db"))
        new_conf.set("path", "pdfpath", os.path.join(libre10_dir, "pdf"))
        new_conf.set("path", "uploadpath", os.path.join(libre10_dir, "pdf-upload"))
        new_conf.set("cache", "cacheFolder", os.path.join(libre10_dir, "cache"))
        new_conf.write(os.path.join(os.path.join(dest_www_dir, "libre10"), "libre10.conf"))

        if not arg_list['--disable-jetty-bin']:
            solr_dir = data_dir + "/libre10/solr"
            f = open(os.path.join(dest_bin_dir, "libre10-solr"), "w")
            if arg_list['--disable-env']:
                solr_script = "#!/bin/sh\nexec java -Djetty.home=%s/server -Dsolr.solr.home=%s/server/solr" \
                              " -jar %s/server/start.jar" % (solr_dir, solr_dir, solr_dir)
            else:
                solr_script = "#!/bin/sh\nexec /usr/bin/env java -Djetty.home=%s/server -Dsolr.solr.home=%s/server/solr" \
                              " -jar %s/server/start.jar" % (solr_dir, solr_dir, solr_dir)
            f.write(solr_script)
            f.close()
            os.chmod(os.path.join(dest_bin_dir, "libre10-solr"), 0755)
        if not arg_list['--disable-wsgi-bin']:
            f = open(os.path.join(dest_bin_dir, "libre10-wsgi"), "w")
            if arg_list['--disable-env']:
                wsgi_script = "#!/bin/sh\npython2 %s/libre10/libre10_wsgi.py" % (www_dir,)
            else:
                wsgi_script = "#!/bin/sh\nexec /usr/bin/env python2 %s/libre10/libre10_wsgi.py" % (www_dir,)
            if arg_list['--python-path']:
                wsgi_script = wsgi_script.replace("python2", arg_list['--python-path'])
            f.write(wsgi_script)
            f.close()
            os.chmod(os.path.join(dest_bin_dir, "libre10-wsgi"), 0755)
        f = open(os.path.join(dest_bin_dir, "libre10"), "w")
        if arg_list['--disable-env']:
            exec_script = "#!/bin/sh\npython2 %s/libre10/libre10_exec.py $@" % (www_dir,)
        else:
            exec_script = "#!/bin/sh\nexec /usr/bin/env python2 %s/libre10/libre10_exec.py $@" % (www_dir,)
        if arg_list['--python-path']:
            exec_script = wsgi_script.replace("python2", arg_list['--python-path'])
        f.write(exec_script)
        f.close()
        os.chmod(os.path.join(dest_bin_dir, "libre10"), 0755)
        if arg_list["--enable-systemd-service"]:
            subprocess.call(["cp", os.path.join(os.path.join(top_dir, "conf"), "systemd-libre10-solr.service"),
                             os.path.join(dest_systemd_dir, "libre10-solr.service")])
            subprocess.call(["cp", os.path.join(os.path.join(top_dir, "conf"), "systemd-libre10-wsgi.service"),
                             os.path.join(dest_systemd_dir, "libre10-wsgi.service")])
    elif arg_list['remove']:
        import shutil

        data_dir = arg_list['--data-dir']
        bin_dir = arg_list['--bin-dir']
        www_dir = arg_list['--www-dir']
        libre10_dir = os.path.join(data_dir, "libre10")
        top_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        subprocess.call(["rm", "-f", os.path.join(bin_dir, 'libre10')])
        subprocess.call(["rm", "-rf", libre10_dir])
        subprocess.call(["rm", "-rf", os.path.join(www_dir, 'libre10')])
        subprocess.call(["rm", "-f", os.path.join(bin_dir, 'libre10-solr')])
        subprocess.call(["rm", "-f", os.path.join(bin_dir, 'libre10-wsgi')])
