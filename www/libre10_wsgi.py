#!/usr/bin/python
# -*- coding:utf-8 -*-
"""Libre10

Libre10 web based pdf search engine
wsgi-server
Copyright (C) 2013-2015 yukikaze

Usage:
    libre10-wsgi [--port=<port>]
    libre10-wsgi -h | --help
    libre10-wsgi --version
    libre10_wsgi.py [--port=<port>]
    libre10_wsgi.py -h | --help
    libre10_wsgi.py --version
Options:
    -h --help   Show this screen.
    --port=<data_dir>       Assign port to libre10-wsgi server [default: 8008]
"""

from __future__ import print_function, unicode_literals
import os
import base64
import datetime
import time

import anyjson

import bottle
import libre10

from bottle import route, post, request, get, template
from docopt import docopt

os.chdir(os.path.dirname(__file__))
application = bottle.default_app()
bottle.debug(True)
bottle.response.content_type = 'application/json; charset=utf-8'
conf = libre10.Config()
base_dir = os.path.dirname(os.path.abspath(__file__))

mstranslator = None
try:
    from microsofttranslator import Translator
    conf.get("optional", "mstranslator_client_id")
    mstranslator = Translator(conf.get("optional", "ms_translator_client_id"), conf.get("optional", "ms_translator_client_secret"))
except:
    mstranslator = None

def datetime_serializer(obj):
    if isinstance(obj, datetime.datetime):
        return obj.strftime("%Y-%m-%d %H:%M:%S")

def session_check(db,session,username="none"):
    if (db.check_session_authorized(session)) and len(username) > 0:
        return 1
    else :
        return 0

@route('/')
@route('/index.html')
def html_index():
    return template('index.tpl', text='', searchOnLoad=0, get_url=application.get_url)


@route('/login.html')
def html_login():
    bottle.response.content_type = "text/html"
    f = open('login.html')
    ret = f.read()
    f.close()
    return ret


@route('/manager.html')
def html_manager():
    bottle.response.content_type = "text/html"
    f = open('manager.html')
    ret = f.read()
    f.close()
    return ret


@route('/jslibs.js')
def js_libs():
    return bottle.static_file('jslibs.js', root='./')


@route('/help/<address:path>')
@route('/libre10.wsgi/help/<address:path>')
def html_help(address='index.html'):
    return bottle.static_file(address, root='./help')


@route('/images/<address:path>')
@route('/libre10.wsgi/images/<address:path>')
def html_images(address='index.html'):
    return bottle.static_file(address, root='./images')


@post('/session')
@post('/libre10.wsgi/session')
def session():
    with libre10.Libre10db(conf.get("path", "dburl")) as db:
        sessionstr = request.forms.get('session')
        if len(sessionstr) > 0:
            if db.check_session_exist(sessionstr):
                if db.check_session(sessionstr):
                    return anyjson.serialize({'result': 1, 'first': db.check_first_user()})
                else:
                    return anyjson.serialize({'result': 0, 'first': db.check_first_user()})
            else:
                return anyjson.serialize(
                    {'result': 0, 'session': db.generate_session_string(bottle.request.remote_addr, 0, int(conf.get("security","login_session_expire_day",7))),
                     'first': db.check_first_user()})
        else:
            return anyjson.serialize({'result': 0, 'session': db.generate_session_string(bottle.request.remote_addr, 0, int(conf.get("security","login_session_expire_day",7))),
                                      'first': db.check_first_user()})


@post('/sessionval')
@post('/libre10.wsgi/sessionval')
def validate_session():
    with libre10.Libre10db(conf.get("path", "dburl")) as db:
        sessionstr = request.forms.get('session')
        if len(sessionstr) > 0:
            return anyjson.serialize({'result': db.check_session_authorized(sessionstr)})


#### login
@post('/login')
@post('/libre10.wsgi/login')
def login():
    with libre10.Libre10db(conf.get("path", "dburl")) as db:
        sessionstr = request.forms.get('session')
        username = request.forms.get('username').lower()
        vinegar = request.forms.get('vinegar')
        sessiondb = db.get_single_session(sessionstr)
        if sessiondb:
            salt = sessiondb['salt']
            cur = db.get_user_by_username(username)
            if len(cur) > 0:
                cur = cur[0]
                if request.forms.get('password'):
                    passhash = request.forms.get('password')
                    if libre10.Security.generate_password_with_salt_hash(cur['password'], salt + vinegar) == passhash:
                        if cur['auth'] == 1:
                            db.set_session_authorized(sessionstr, 1, username)
                            db.close()
                            time.sleep(0.5)
                            return anyjson.serialize({'result': 1})
                        else:
                            # return anyjson.serialize({'result': 0, 'message': 'please contact admin to auth you.'})
                            return anyjson.serialize(
                                {'result': 0, 'message': 'please contact admin to auth you.'})
                    else:
                        return anyjson.serialize({'result': 0, 'message': 'user/password is not correct.'})
                else:
                    db.close()
                    time.sleep(0.5)
                    return anyjson.serialize(
                        {'result': 2, 'sugar': cur['sugar'], 'passhash': cur['password'], 'vinegar': vinegar})
            else:
                return anyjson.serialize({'result': 0, 'message': 'user/password is not correct.'})


#### manager
@post('/adduser')
@post('/libre10.wsgi/adduser')
def add_user():
    with libre10.Libre10db(conf.get("path", "dburl")) as db:
        sessionstr = request.forms.get('session')
        username = request.forms.get('username').lower()
        password = request.forms.get('password')
        if (db.check_session(sessionstr) or db.check_first_user()) and len(username) > 0 and len(password) > 0:
            sugar = libre10.Security.generate_salt()
            db.add_user(username, sugar, libre10.Security.generate_password_hash(sugar, password),
                        db.check_first_user())
            return anyjson.serialize({'result': 1})
        else:
            return anyjson.serialize({'result': 0})


@post('/authuser')
@post('/libre10.wsgi/authuser')
def auth_user():
    with libre10.Libre10db(conf.get("path", "dburl")) as db:
        if session_check(db,request.forms.get('session'), request.forms.get('username')):
            db.auth_user(request.forms.get('username'))



@post('/deluser')
@post('/libre10.wsgi/deluser')
def del_user():
    with libre10.Libre10db(conf.get("path", "dburl")) as db:
        if session_check(db,request.forms.get('session'), request.forms.get('username')):
            db.del_user(request.forms.get('username'))


@get('/pdflist')
@get('/libre10.wsgi/pdflist')
def print_pdflist():
    with libre10.Libre10db(conf.get("path", "dburl")) as db:
        if session_check(db,request.params['session']):
            return anyjson.serialize({"pdflist_group": db.get_pdffile_sort_by_title()})


@get('/pdfuserlist')
@get('/libre10.wsgi/pdfuserlist')
def print_pdflist():
    with libre10.Libre10db(conf.get("path", "dburl")) as db:
        if session_check(db,request.params['session']):
            return anyjson.serialize({"pdflist_user_group": db.get_pdffile_user_sort_by_title()})


@get('/genrelist')
@get('/libre10.wsgi/genrelist')
def pring_genre():
    with libre10.Libre10db(conf.get("path", "dburl")) as db:
        if session_check(db,request.params['session']):
            return anyjson.serialize({"genrelist_group": db.get_genre_sort_by_genre()})

@get('/userlist')
@get('/libre10.wsgi/userlist')
def print_userlist():
    with libre10.Libre10db(conf.get("path", "dburl")) as db:
        if session_check(db,request.params['session']):
            return anyjson.serialize({'userlist_group': db.get_user_sort_by_id(),
                                      'sessionlist_group': db.get_session_sort_by_id()})


@get('/thumblist')
@get('/libre10.wsgi/thumblist')
def print_pdflist():
    ret = {}
    with libre10.Libre10db(conf.get("path", "dburl")) as db:
        sessionstr = request.params['session']
        genrestr = unicode(request.params['genre'], "utf-8")
        if session_check(db,sessionstr):
            dbtmp = db.get_single_session(sessionstr)
            username = dbtmp["username"]
            genrelist2 = db.get_titles_for_user_from_pdffile_user_by_genre(username, genrestr)
            genrelist = db.get_titles_from_pdffile_by_genre(genrestr)
            if len(genrelist) > 20:
                genrelist = genrelist[:20]
            ret["thumbs_groups"] = genrelist
            ret["thumbs_user_groups"] = genrelist2
    return anyjson.serialize(ret)


@post('/upload')
@post('/libre10.wsgi/upload')
def upload():
    with libre10.Libre10db(conf.get("path", "dburl")) as db:
        if session_check(db,request.forms.get('session')):
            upload_file = request.files.get('file')
            file_name = unicode(upload_file.raw_filename, 'utf-8')
            ext = os.path.splitext(file_name)[1]
            if ext not in ('.pdf', '.PDF', '.Pdf'):
                return 'File extension not allowed.'
            save_path = conf.get("path", "uploadpath")
            upload_file.save(os.path.join(save_path, file_name).encode("utf-8"))
            return 'OK'
        return "Login needed."


@post('/manage')
@post('/libre10.wsgi/manage')
def manage():
    with libre10.Libre10db(conf.get("path", "dburl")) as db:
        if session_check(db,request.forms.get('session')):
            modestr = request.forms.get('mode')
            sc = libre10.SolrAccess(db, conf.get("path", "solrurl"))
            if modestr == 'addgenre':
                title_id = request.forms.get('titleid')
                db.add_genre_text(title_id, unicode(request.forms.get('genre'), 'utf-8'))
            elif modestr == 'delete':
                title_id = request.forms.get('titleid')
                ret = []
                pj = libre10.PdfToJpeg(conf.get("cache", 'cacheFolder'))
                if len(title_id) > 0:
                    titlelist = db.get_pdffile_by_title_id(title_id)
                    for title in titlelist:
                        sc.delete_title_group(title["title"])
                        ret.append(db.delete_pdffile_by_title_id(title["title_id"]))
                        db.delete_genre_by_title_id(title["title_id"])
                        pj.delete_pdf_cache_files(title["path"])
                        try:
                            os.remove(title["path"])
                        except:
                            ""
                return anyjson.serialize(ret)
            elif modestr == 'setpdfdesc':
                title_id = unicode(request.forms.get('titleid'), 'utf-8')
                spd = request.forms.get('spd')
                indexpage = request.forms.get('indexpage')
                db.set_pdffile_description(title_id, spd, indexpage)
            elif modestr == 'check_solr_version':
                return anyjson.serialize(db.get_status())
            elif modestr == 'clear_solr':
                sc.delete_all()
                db.set_pdffile_changed()
                ##TODO:set pdf table changed
            elif modestr == 'index':
                import subprocess

                subprocess.call("echo 'libre10 import'|batch", shell=True)
                return anyjson.serialize({'exec': 1})
            elif modestr == 'check_update':
                if db.check_latest():
                    return anyjson.serialize({'latest': 1})
                else:
                    return anyjson.serialize({'latest': 0, 'docNum': sc.get_document_num()})


@get('/backup')
@get('/libre10.wsgi/backup')
def backup():
    import datetime

    with libre10.Libre10db(conf.get("path", "dburl")) as db:
        if session_check(db,request.params['session']):
            retstr = db.get_db_json()
            bottle.response.headers[
                'Content-Disposition'] = "attachment; filename=libre10-backup-" + datetime.datetime.now().strftime(
                "%Y-%m-%d") + ".json"
            return retstr


@post('/restore')
@post('/libre10.wsgi/restore')
def restore():
    with libre10.Libre10db(conf.get("path", "dburl")) as db:
        if session_check(db,request.forms.get('session')):
            upload_file = request.files.get('file')
            file_name = unicode(upload_file.raw_filename, 'utf-8')
            ext = os.path.splitext(file_name)[1]
            if ext not in ('.json', '.JSON'):
                return 'File extension not allowed.'
            list = anyjson.deserialize(upload_file.file.read())
            db.set_db_from_json(list)
            return anyjson.serialize(list)
        return "Login needed."


#### search
@post('/search')
@post('/libre10.wsgi/search')
def search():
    import time

    start_time = time.time()
    rettime = []
    with libre10.Libre10db(conf.get("path", "dburl")) as db:
        rettime.append({'r1': time.time() - start_time})
        if session_check(db,request.forms.get('session')):
            sc = libre10.SolrAccess(db, conf.get("path", "solrurl"))
            requestd = request.forms.decode()
            searchtext = requestd.get('text')
            genre = requestd.get('genre')
            titleid = requestd.get('titleid')
            start = 0
            if requestd.get('start'):
                start = int(requestd.get('start'))
            ret = {'genre_group': [], }
            ret['start'] = start
            rettime.append({'r2': time.time() - start_time})
            for lst in db.get_genre_group_by_genre():
                if len(lst['genre']) > 0:
                    import random

                    random.seed(lst['genre'])
                    ret['genre_group'].append({
                        'labelcolor': ["label-primary", "label-success", "label-info", "label-warning", "label-danger"][
                            random.randint(0, 4)],
                        'genre': lst['genre']})
                    ret['show_genre'] = 1
            rettime.append({'r2.1': time.time() - start_time})
            if len(searchtext) > 0:
                if mstranslator:
                    ret['mstranslator'] = mstranslator.translate(searchtext,'en').lower()
                    searchtext = "\"" + searchtext + "\" OR \"" + ret['mstranslator'] + "\""

                if len(genre) > 0:
                    query = sc.search_query(searchtext,
                                            sc.lists_dic_to_solr_query_string(
                                                db.get_titles_from_pdffile_by_genre(genre),
                                                "title"), start=str(start))
                    ret["genrelist"] = db.get_titles_from_pdffile_by_genre(genre)
                    ret['fq_genre'] = genre
                    rettime.append({'r2.2': time.time() - start_time})
                elif len(titleid) > 0:
                    query = sc.search_query(searchtext,
                                            sc.lists_dic_to_solr_query_string(
                                                db.get_titles_from_pdffile_by_titleid(titleid), "title"),
                                            start=str(start))
                    rettime.append({'r2.3': time.time() - start_time})
                else:
                    query = sc.search_query(searchtext, start=str(start))
                rettime.append({'r3': time.time() - start_time})
                group = sc.solr_response_to_html_group(query)
                #ret['query'] = query #for debug fixme
                ret['group'] = group
                ret['show_result'] = 1

            if len(genre) > 0:
                ret['genre_mode'] = 1
                import random

                random.seed(genre)
                ret['genre_labelcolor'] = \
                    ["label-primary", "label-success", "label-info", "label-warning", "label-danger"][
                        random.randint(0, 4)]
                ret['genre_text'] = genre
            rettime.append({'r5': time.time() - start_time})
            ret["time"] = rettime
            return anyjson.serialize(ret)


# REST

@route('/search/<text>/', method="GET")
@route('/libre10.wsgi/search/<text>/', method="GET")
def rest_html_search(text=''):
    return template('index.tpl', text=text, searchOnLoad=1, get_url=application.get_url)


@route('/view/<title>/<page>/', method="GET")
@route('/libre10.wsgi/view/<title>/<page>/', method="GET")
def pdf_view_site(title="", page=0):
    return template('view.tpl', title=title, pagenum=page, get_url=application.get_url)


@route('/libre10.wsgi/getimage/<width:float>/<titlestr>/<pagenum:int>/<is_light:int>/')
@route('/getimage/<width:float>/<titlestr>/<pagenum:int>/<is_light:int>/')
def rest_view_pdf(width=1000, titlestr="title", pagenum=1, is_light=0):
    with libre10.Libre10db(conf.get("path", "dburl")) as db:
        if session_check(db,request.headers.get('X-LIBRE10-SESSION')):
            title = unicode(titlestr, 'utf-8')
            width = int(float(width))
            pj = libre10.PdfToJpeg(conf.get("cache", 'cacheFolder'))
            pdfdata = db.search_pdffile_by_title_pagenum(title, pagenum)
            ret = {'nowpage': pagenum, 'booktitle': pdfdata['title'], 'nowspd': pdfdata['showpagediff'],
                   'indexpage': pdfdata['indexpage'], 'querystr': request.query_string}
            dbtmp = db.get_single_session(request.headers.get('X-LIBRE10-SESSION'))
            username = dbtmp["username"]
            ret["username"] = dbtmp
            if is_light:
                rettmp = pj.get_jpeg_string_from_pdf(pdfdata['path'], str(int(pagenum) - int(pdfdata['startpage'])),
                                                     width,
                                                     is_light)
                ret['img1'] = base64.b64encode(rettmp[0])
                ret['img1_result'] = rettmp[1]
                # ret['img1_debug'] = rettmp[2]#fixme
            else:
                rettmp = pj.get_jpeg_string_from_pdf(pdfdata['path'], str(int(pagenum) - int(pdfdata['startpage'])),
                                                     width, is_light)
                ret['img1'] = base64.b64encode(rettmp[0])
                ret['img1_result'] = rettmp[1]
                rettmp = pj.get_jpeg_string_from_pdf(pdfdata['path'], str(int(pagenum) - int(pdfdata['startpage']) + 1),
                                                     width, is_light)
                ret['img2'] = base64.b64encode(rettmp[0])
                ret['img2_result'] = rettmp[1]
                ret['light'] = 0
            db.add_count_from_pdffile_user(username, pdfdata["title_id"], int(pagenum))
            return anyjson.serialize(ret)
        else:
            ret = {'session': request.headers.get('X-LIBRE10-SESSION')}
            return anyjson.serialize(ret)


@route('/libre10.wsgi/getpos/<titlestr>/<pagenum:int>/<searchtext>/')
@route('/getpos/<titlestr>/<pagenum:int>/<searchtext>/')
def rest_pos_pdf(titlestr="title", pagenum=1, searchtext=""):
    with libre10.Libre10db(conf.get("path", "dburl")) as db:
        if session_check(db,request.headers.get('X-LIBRE10-SESSION')):
            title = unicode(titlestr, 'utf-8')
            search_text = unicode(searchtext, 'utf-8')
            pdfdata = db.search_pdffile_by_title_pagenum(title, pagenum)
            ret = {'nowpage': pagenum, 'booktitle': pdfdata['title'], 'nowspd': pdfdata['showpagediff'],
                   'indexpage': pdfdata['indexpage'], 'querystr': request.query_string}
            ptt = libre10.PdfToText(conf.get("cache", "cacheFolder"))
            rettmp2 = ptt.get_text_postion_from_pdf_texts(search_text, pdfdata['path'],
                                                          str(int(pagenum) - int(pdfdata['startpage'])),
                                                          conf.get("cache", 'cacheFolder'))
            ret['pos_all'] = rettmp2
            ret['pos'] = rettmp2['pos']
            return anyjson.serialize(ret)


@route('/libre10.wsgi/search/<genrestr>/<titleid>/<searchtext>/<start:int>/')
@route('/search/<genrestr>/<titleid>/<searchtext>/<start:int>/')
def rest_search(titleid="", genrestr="", searchtext="", start=0):
    import time

    start_time = time.time()
    rettime = []
    with libre10.Libre10db(conf.get("path", "dburl")) as db:
        rettime.append({'r1': time.time() - start_time})
        if session_check(db,request.headers.get('X-LIBRE10-SESSION')):
            sc = libre10.SolrAccess(db, conf.get("path", "solrurl"))
            genre = genrestr
            ret = {'genre_group': [], }
            ret['start'] = start
            rettime.append({'r2': time.time() - start_time})
            for lst in db.get_genre_group_by_genre():
                if len(lst['genre']) > 0:
                    import random

                    random.seed(lst['genre'])
                    ret['genre_group'].append({
                        'labelcolor': ["label-primary", "label-success", "label-info", "label-warning", "label-danger"][
                            random.randint(0, 4)],
                        'genre': lst['genre']})
                    ret['show_genre'] = 1
            rettime.append({'r2.1': time.time() - start_time})
            if len(searchtext) > 0:
                if len(genre) > 0:
                    query = sc.search_query(searchtext,
                                            sc.lists_dic_to_solr_query_string(
                                                db.get_titles_from_pdffile_by_genre(genre),
                                                "title"), start=str(start))
                    ret["genrelist"] = db.get_titles_from_pdffile_by_genre(genre)
                    ret['fq_genre'] = genre
                    rettime.append({'r2.2': time.time() - start_time})
                elif len(titleid) > 0:
                    query = sc.search_query(searchtext,
                                            sc.lists_dic_to_solr_query_string(
                                                db.get_titles_from_pdffile_by_titleid(titleid), "title"),
                                            start=str(start))
                    rettime.append({'r2.3': time.time() - start_time})
                else:
                    query = sc.search_query(searchtext, start=str(start))
                rettime.append({'r3': time.time() - start_time})
                group = sc.solr_response_to_html_group(query)
                # ret['query'] = query #for debug fixme
                ret['group'] = group
                ret['show_result'] = 1
            if len(genre) > 0:
                ret['genre_mode'] = 1
                import random

                random.seed(genre)
                ret['genre_labelcolor'] = \
                    ["label-primary", "label-success", "label-info", "label-warning", "label-danger"][
                        random.randint(0, 4)]
                ret['genre_text'] = genre
            rettime.append({'r5': time.time() - start_time})
            ret["time"] = rettime
            return anyjson.serialize(ret)


@route('/libre10.wsgi/getthumbs/<titlestr>/<pagenum:int>/')
@route('/getthumbs/<titlestr>/<pagenum:int>/')
def rest_get_thumbs(titlestr="title", pagenum=1):
    with libre10.Libre10db(conf.get("path", "dburl")) as db:
        if session_check(db,request.headers.get('X-LIBRE10-SESSION')):
            title = unicode(titlestr, 'utf-8')
            pj = libre10.PdfToJpeg(conf.get("cache", 'cacheFolder'))
            pdfdata = db.search_pdffile_by_title_pagenum(title, pagenum)
            ret = {'nowpage': pagenum, 'booktitle': pdfdata['title'], 'nowspd': pdfdata['showpagediff'],
                   'indexpage': pdfdata['indexpage'], 'querystr': request.query_string, 'title_id': title,
                   'nowpage': pagenum}
            rettmp = pj.get_jpeg_string_from_pdf(pdfdata['path'], str(int(pagenum) - int(pdfdata['startpage'])),
                                                 200, 1)
            ret['img'] = base64.b64encode(rettmp[0])

            return anyjson.serialize(ret)
        else:
            ret = {'session': request.headers.get('X-LIBRE10-SESSION')}
            return anyjson.serialize(ret)


def html_images(address='index.html'):
    return bottle.static_file(address, root='./images')


if __name__ == '__main__':
    arg_list = docopt(__doc__, version="1.8.0")  # fixme version
    print(arg_list)
    port_number = int(arg_list['--port'])
    bottle.run(host='0.0.0.0', server='auto', port=port_number, debug=True, reloader=True)
