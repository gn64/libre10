<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="ja-JP" xml:lang="ja-JP">
<!--
# Libre10 web based pdf search engine
# Copyright (C) 2013-16 yukikaze
-->
<head>
    <meta charset="utf-8"/>
    <title>Libre10 pdf search -search-</title>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/tether/1.1.1/css/tether.min.css">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <style type="text/css">
    </style>
    <!--[if lt IE 9]>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <![endif]-->
    <!--[if gte IE 9]><!-->
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <!--<![endif]-->
    <script type="text/javascript" src="//ajax.aspnetcdn.com/ajax/knockout/knockout-3.4.0.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/knockout.mapping/2.4.1/knockout.mapping.min.js"></script>
    <script type="text/javascript" src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/js-cookie/2.1.0/js.cookie.min.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no" />
    <meta http-equiv="Expires" content="3600">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" sizes="57x57" href="images/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="images/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="images/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="images/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="images/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="images/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="images/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="images/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="images/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="images/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="images/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="images/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon-16x16.png">
    <link rel="manifest" href="images/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="images/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <script type="text/javascript">
        $(function(){
            var searchResults;
            var showUpdater=false;
            var ua=window.navigator.userAgent.toLowerCase();
            var isMobile = (ua.indexOf('android') > 0 || ua.indexOf('iphone') > 0)? 1:0;
            function removeItemFromStorage(keystr){
                if (Modernizr.localstorage) {
                    return localStorage.removeItem(keystr);
                } else {
                    Cookies.remove("libre10-"+keystr);
                }
            }
            function setItemToStorage(keystr,value){
                if (Modernizr.localstorage){
                    return localStorage.setItem(keystr, value);
                }else{
                    Cookies.set("libre10-"+keystr,value,{expires:7});
                }
            }
            function getItemFromStorage(keystr){
                if (Modernizr.localstorage){
                    return localStorage.getItem(keystr);
                }else{
                    return Cookies.get("libre10-"+keystr);
                }
            }
            function checkSession(){
                if(getItemFromStorage("session")){//セッションあり
                    session_id = getItemFromStorage("session");
                    console.log("session is there.");
                    $.post('{{get_url('/')}}libre10.wsgi/sessionval',{session: session_id},function(data){;;
                        isSessionSigned = ( data.result == 1 );
                        if (! isSessionSigned) {//有効なセッションでない場合
                            history.pushState('','',window.location.href);
                            window.location.href = "{{get_url('/')}}login.html";
                        }else{
                            console.log("session validated.");
                            $.ajaxSetup({
                                headers: { "X-LIBRE10-SESSION": session_id }
                            });
                        }
                    },"json");
                }else{//セッションなしの場合
                    history.pushState('','',window.location.href);
                    window.location.href = "{{get_url('/')}}login.html";;
                }
            }
            function checkUpdate(){
                $.ajax({
                    type:"post",
                    url:"{{get_url('/')}}manage",
                    data:{session:getItemFromStorage("session"),mode:'check_update'},
                    success:function(data){
                        if (data.latest == 1){
                            console.log("check latest fin.");
                        }else if(data.latest == 0){
                            console.log("check latest fin.You need to update.");
                            showUpdater = true;
                        }
                    },
                    dataType:"json",
                    async:false
                });
            }
            $(document).ready(function(){
                $(window).scroll(function(){
                    if($(this).scrollTop() > 100){
                        $("#pageTopL").fadeIn();
                        $("#pageTopR").fadeIn();
                    }else{
                        $("#pageTopL").fadeOut();
                        $("#pageTopR").fadeOut();
                    }
                });
                $("#pageTopL").click(function(){
                    $("body,html").animate({scrollTop:0},'fast');
                });
                $("#pageTopR").click(function(){
                    $("body,html").animate({scrollTop:0},'fast');
                });
                $(document).keydown(function(event){
                    if (event.target.tagName.toLowerCase() !== 'input' && event.which == 70){
                        $("body,html").animate({scrollTop:0},'fast');
                    }
                });
                $(document).keypress(function(event){
                    //console.log(event.target.tagName);
                    //console.log(event.which);
                    if (event.target.tagName.toLowerCase() == 'input' && event.which == 13){
                        window.searchButtonClicked();
                        $("body,html").animate({scrollTop:0},'fast');
                    }
                });
                //knockdown.js
                var defaultJS={"group":[], "genre_group":[], "defaultlink":"", "show_result":0, "show_genre":0,
                    "genre_mode":0, "search_top":1, "genre_labelcolor":"", "text":"{{text}}", "genre_text":"", "row":20,
                    "start":0, "dtime":"", "searchStart":null, "show_thumbs":1, "infotext": "", "micButtonEnabled":0};

                //knockdown init check
                if ('webkitSpeechRecognition' in window) {
                    defaultJS["micButtonEnabled"] = 1;
                }

                function myViewModel(data){
                    var self = this;
                    ko.mapping.fromJS(data,{},self);
                    if (isMobile == 1){
                        self.show_thumbs(0);
                    }
                    self.show_alert_loading = ko.observable(false);
                    self.showInfo = ko.observable(false);
                    self.showUpdater=ko.observable(showUpdater);
                    self.thumbs_groups = ko.observable([]);
                    self.thumbs_group = ko.observableArray();
                    self.thumbs_user_groups = ko.observable([]);
                    self.thumbs_user_group = ko.observableArray([]);
                    self.thumbs_group_calc = ko.computed(function(){
                        self.thumbs_group.removeAll();
                        tmpgroup=self.thumbs_groups();
                        ret = [];
                        if (tmpgroup){
                            for (var i=0;i < tmpgroup.length;i++) {
                                if (!tmpgroup[i]['nowpage']){
                                    tmpgroup[i]['nowpage']=1;
                                }
                                address_str="{{get_url('/')}}getthumbs/" + tmpgroup[i]['title_id'] + "/" + tmpgroup[i]['nowpage'] + "/";
                                var thumb_tmp =ko.computed(function(){
                                    $.ajax({
                                        type: 'GET',
                                        url: address_str,
                                        cache: false,
                                        dataType: "json",
                                        timeout: 8000 // in milliseconds
                                    }).done(function(data){
                                        data["i"]=i;
                                        var tmp = ko.mapping.fromJS(data);
                                        if (self.thumbs_group().length <= i){
                                            self.thumbs_group.push(tmp);
                                        }else{
                                            self.thumbs_group[i]=tmp;
                                        }
                                    });
                                }).extend({async:true});
                                ret.push(thumb_tmp);
                            }
                        }
                        return ret;
                    });
                    self.thumbs_user_group_calc = ko.computed(function(){
                        self.thumbs_user_group.removeAll();
                        tmpgroup=self.thumbs_user_groups();
                        if (tmpgroup){
                            for (var i=0;i < tmpgroup.length;i++) {
                                address_str="{{get_url('/')}}getthumbs/" + tmpgroup[i]['title_id'] + "/" + tmpgroup[i]['nowpage'] + "/";
                                var thumb_tmp =ko.computed(function(){
                                    $.ajax({
                                        type: 'GET',
                                        url: address_str,
                                        cache: false,
                                        dataType: "json",
                                        timeout: 8000 // in milliseconds
                                    }).done(function(data){
                                        data["i"]=i;
                                        var tmp = ko.mapping.fromJS(data);
                                        if (self.thumbs_user_group().length <= i){
                                            self.thumbs_user_group.push(tmp);
                                        }else{
                                            self.thumbs_user_group[i]=tmp;
                                        }
                                    });
                                }).extend({async:true});
                                ret.push(thumb_tmp);
                            }
                        }
                        return ret;
                    });
                    console.log(self.showUpdater());
                    self.jumpUpdater = function(){
                        w = window.open("{{get_url('/')}}manager.html");
                        $(w).on('load',function(){
                            w.showIndexer();
                        });
                        //window.location.href = "manager.html";
                    };
                    self.searchButtonClicked = function(){
                        self.searchStart(new Date());
                        self.show_alert_loading(true);
                        //console.log(self.text());
                        if (self.show_genre() > 0){
                            updateJSONpost(self.text(),self.genre_text(),"")
                        }else{
                            updateJSONpost(self.text(),"","")
                        }
                    };window.searchButtonClicked = self.searchButtonClicked;
                    self.micButtonClicked = function() {
                        var nowRecognition = false;
                        // unsupported.
                        if (!'webkitSpeechRecognition' in window) {
                            self.infotext("This browser does not support WebSpeech API");
                            self.showInfo(1);
                            return;
                        }
                        if (nowRecognition) {
                            nowRecognition = false;
                            recognition.stop();
                            self.micButtonEnabled(1);
                        } else {
                            // star audio recognition
                            recognition = new webkitSpeechRecognition();
                            recognition.lang = "ja-JP";//en-US
                            recognition.onresult = function (e) {
                                if (e.results.length > 0) {
                                    var value = e.results[0][0].transcript;
                                    console.log(e.results[0][0]);
                                    if (e.results[0][0].confidence > 0.5) {
                                        self.text(value);
                                        self.searchButtonClicked();
                                    }else {
                                        self.infotext("Speech input confidence is too low. Try agein please.");
                                        self.showInfo(1);
                                    }
                                    nowRecognition = false;
                                    recognition.stop();
                                    self.micButtonEnabled(1);
                                }
                            };
                            recognition.start();
                            nowRecognition = true;
                            self.micButtonEnabled(0);
                            setTimeout(function(){
                                nowRecognition = false;
                                recognition.stop();
                                self.micButtonEnabled(1);
                            },10000);
                        }
                    };
                    self.allgenreButtonClicked = function(){
                        self.searchStart(new Date());
                        self.show_alert_loading(true);
                        self.genre_mode(0);
                        self.show_genre(0);
                        self.genre_text("");
                        updateJSONpost(self.text(),"","");
                        self.getThumbs("");
                    };
                    self.genreButtonClicked = function(genre){
                        self.searchStart(new Date());
                        self.show_alert_loading(true);
                        updateJSONpost(self.text(),genre,"");
                        self.getThumbs(genre);
                    };
                    self.viewButtonClicked = function(title,pagenum){
                        if (Number(pagenum) == 0){
                            pagenum = 1;
                        }
                        address = "{{get_url('/')}}view/"+title+"/"+pagenum+"/";
                        w = window.open(address);
                        $(w).on('load',function(){
                            w.setSearchResults(searchResults,self.text());
                            w.setSearchVars(title,pagenum);
                            w.setSearchJump();
                        });
                    };
                    self.hideAlert = function(){
                        self.show_alert_loading(false);
                    };window.hideAlert=self.hideAlert;
                    self.nextResult = function(){
                        self.searchStart(new Date());
                        self.show_alert_loading(true);
                        if (self.show_genre() > 0){
                            updateJSONpostStart(self.text(),self.genre_text(),"",self.start()+self.row());
                        }else{
                            updateJSONpost(self.text(),"","",self.start()+self.row());
                        }
                        $("body,html").animate({scrollTop:0},'fast');
                    };
                    self.lastResult = function(){
                        self.searchStart(new Date());
                        new_start = self.start() - self.row();
                        if(new_start<0){
                            new_start = 0;
                        }
                        self.show_alert_loading(true);
                        if (self.show_genre() > 0){
                            updateJSONpostStart(self.text(),self.genre_text(),"",new_start);
                        }else{
                            updateJSONpost(self.text(),"","",new_start);
                        }
                        $("body,html").animate({scrollTop:0},'fast');
                    };
                    self.checkTime = function(){
                        currentTime = new Date();
                        self.dtime((currentTime - self.searchStart())/1000+"sec");
                    };window.checkTime = self.checkTime;
                    self.getThumbs = function(genre){
                        $.ajax({
                            type: 'GET',
                            url: '{{get_url('/')}}thumblist',
                            cache: false,
                            data: {
                                session:getItemFromStorage("session"),
                                genre:genre
                            },
                            dataType: "json"
                        }).done(function(data){
                            self.thumbs_user_groups(data["thumbs_user_groups"]);
                            self.thumbs_groups(data["thumbs_groups"]);
                        });
                    };window.getThumbs = self.getThumbs;
                }
                function updateJSONpostStart(textstr,genrestr,titleidstr,start){
                    var postdata={session: getItemFromStorage("session"),text:textstr,genre:genrestr,titleid:titleidstr,start:start}
                    $.post("{{get_url('/')}}search",postdata,function(data){
                        searchResults = data['group'];
                        ko.mapping.fromJS(data,viewModel);
                    },"json").done(function(){
                        window.hideAlert();
                        window.checkTime();
                    });
                }
                function updateJSONpost(textstr,genrestr,titleidstr){
                    updateJSONpostStart(textstr,genrestr,titleidstr,0);
                }
                checkSession();
                var viewModel=new myViewModel(defaultJS);
                ko.applyBindings(viewModel);
                updateJSONpost("","","");
                if (isMobile != 1){
                    window.getThumbs("");
                }
                if ({{searchOnLoad}}==1)
                {
                    window.searchButtonClicked();
                }
                //$('input_text').focus();
            });;
            checkUpdate();
        })
    </script>
</head>
<body>
<form data-bind="submit: searchButtonClicked">
    <div style="margin:0 auto;text-align:center"><br>
        <div class="hidden-xs-down">
            <h1 style="display:inline;">Libre10 - pdf search tool - </h1>
            <h4 style="display:inline;">
                <span class="label label-default" style="margin:0 auto;float:none;width:20%">
                    1.8.0
                    <!--#todo:fix version number-->
                </span>
                <span class="label label-danger">
                    <a href="{{get_url('/')}}manager.html" style="color:white"><i class="fa fa-cog"></i></a>
                </span>
                <span class="label label-success">
                    <a href="{{get_url('/')}}help/usage_search_en.html#search-screen" style="color:white" target="_blank"><i class="fa fa-question-circle"></i></a>
                </span>
                <span class="label label-warning">
                    <a href="{{get_url('/')}}login.html" style="color:white"><i class="fa fa-unlock-alt"></i></a>
                </span>
            </h4>
        </div>
        <div class="hidden-sm-up">
            <h4 style="display:inline;">
                <span>
                    Libre10
                </span>
                <span class="label label-warning">
                    <a href="{{get_url('/')}}login.html" style="color:white"><i class="fa fa-unlock-alt"></i></a>
                </span>
            </h4>

        </div>
    </div>
    <div style="text-align: right;font-size: 1px;margin-right: 5%"><!--ko text:dtime--><!--/ko--></div>
    <div style="margin:0 auto;text-align:center">
            <span data-bind="if: genre_mode">
                <h4>
                    <span style="color:white" class="label" data-bind="css: genre_labelcolor">
                        <span data-bind="text: genre_text"></span>
                    </span>
                </h4>
                <br>
            </span>
        <br>
    </div>
    <div>
        <div style="margin-left:5%;margin-right:5%;width:100%;text-align: center;">
            <p>
            <div style="width:90%">
                <div class="input-group hidden-xs-down">
                    <input type="text" id="input_text" class="form-control" autofocus data-bind="value: text, valueUpdate: 'afterkeydown'" />
                    <div class="input-group-btn">
                        <button type="button" class="btn btn-warning" data-bind="click: micButtonClicked,enable: micButtonEnabled">
                            &nbsp;
                            <i class="fa fa-microphone"></i>
                            &nbsp;
                        </button>
                        <button type="button" type='submit' class="btn btn-primary" data-bind="click: searchButtonClicked">
                            &nbsp;
                            <i class="fa fa-search"></i>
                            &nbsp;
                        </button>
                        <a target="_blank" class="btn btn-success" data-bind="attr:{href: 'https://www.google.co.jp/search?q='+text()}">
                            <i class="fa fa-google"></i>
                        </a>
                    </div><!-- /btn-group -->
                </div><!-- /input-group -->
                <div class="hidden-sm-up">
                    <input type="text" id="input_text" class="form-control" autofocus data-bind="value: text, valueUpdate: 'afterkeydown'" />
                    <row>
                        <button type="button" class="btn btn-warning col-xs-6" data-bind="click: micButtonClicked,enable: micButtonEnabled">
                            &nbsp;
                            <i class="fa fa-microphone"></i>
                            &nbsp;
                        </button>
                        <button type="button" class="btn btn-primary col-xs-6" data-bind="click: searchButtonClicked">
                            &nbsp;
                            <i class="fa fa-search"></i>
                            &nbsp;
                        </button>
                    </row>
                </div><!-- /input-group -->
            </div>
            </p>
            <br>
            <p style="margin:0 auto;text-align:center">
            <div class="row" data-bind="if: show_genre" style="width:90%;line-height:150%">
                <h4>
                            <span class="label label-default" style="margin:1px auto;float:none;width:20%;display: inline-block;">
                                <a style="color:white" data-bind="click: allgenreButtonClicked">All</a>
                            </span>
                    <!-- ko foreach: genre_group -->
                            <span class="label" style="margin:1px auto;float:none;width:100px;display: inline-block;" data-bind="css: labelcolor">
                                <a style="color:white" data-bind="text: genre, click: function(){$parent.genreButtonClicked(genre)}"></a>
                            </span>
                    <!-- /ko -->
                </h4>
            </div>
            </p>
        </div>
        <br>
            <span  data-bind="visible: show_alert_loading">
                <div class="alert alert-warning" role="alert" style="width:100%;text-align:center">
                    <i class="fa fa-spin fa-spinner"></i>
                </div>
            </span>
        <div data-bind="if: showUpdater">
            <div class="alert alert-danger" role="alert">
                <strong>Warning!</strong>
                Solr schema is obsolute.You need to clear/re-index db.
                <a href='' data-bind='click:jumpUpdater'>update</a>
            </div>
        </div>
        <div data-bind="if: showInfo">
            <div class="alert alert-info" role="alert">
                <!-- ko text: infotext -->
                <!-- /ko -->
            </div>
        </div>
    </div>
        <span data-bind="if: show_result">
            <nav class="navbar-xs navbar-inverse" role="navigation">
                <p><div class="row" style="text-align:center;">
                <div class="hidden-xs-down">
                    <div class="container-fluid">
                        <a class="btn btn-warning m-t-1" data-bind="click:lastResult"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
                        <!-- ko foreach:group -->
                        <a class="btn btn-primary m-t-1" data-bind="attr:{href: '#'+text()}, html: text"></a>
                        <!-- /ko -->
                        <a class="btn btn-warning m-t-1" data-bind="click:nextResult"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
                    </div>
                </div>
                <div class="row hidden-sm-up">
                    <a class="btn btn-warning col-xs-12" data-bind="click:lastResult"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
                    <!-- ko foreach:group -->
                    <a class="btn btn-primary col-xs-12" data-bind="attr:{href: '#'+text()}, html: text"></a>
                    <!-- /ko -->
                    <a class="btn btn-warning col-xs-12" data-bind="click:nextResult"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
                </div>
            </div></p>
            </nav>
            <div data-bind="foreach:{data: group()}">
                <div class="card card-inverse">
                    <!--<div class="card-header" data-bind="attr:{id: text}">-->
                    <div class="card-header card-success card-inverse" data-bind="attr:{id: text}">
                        <h4><span data-bind="text: text"></span>
                            <span class="label label-pill" data-bind="text: search_num"></span>
                            <a href="#top" class="btn btn-warning">top</a>
                        </h4>
                    </div>
                </div>
                <div class="card" data-bind="foreach: search_result">
                    <div class="card">
                        <div class="card-block">
                            <h4 class="card-title">
                                <span data-bind="text: 'page' + page()"></span>
                            </h4>
                            <p class="card-text" style="word-break:break-all;" data-bind="html: hl_text"></p>
                            <button type="button" class="btn btn-danger" data-bind="click: function(){$root.viewButtonClicked(title(),page());}">view</button>
                            <!--<button type="button" class="btn btn-danger" data-bind="click: function(){$root.viewButtonClicked(title(),page());}">view</button>-->

                        </div>
                    </div>
                </div>
            </div>
            <nav>
                <ul class="pager">
                    <li class="previous"><a href="" data-bind="click:lastResult">Last</a></li>
                    <li class="next"><a href="" data-bind="click:nextResult">Next</a></li>
                </ul>
            </nav>
        </span>
        <span data-bind="if: show_thumbs">
            <div class="row" id="thumbs_user">
                <br>
                <div class="">
                    <!-- ko foreach: thumbs_user_group -->
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-3 col-xl-3">
                        <div class="card card-warning">
                            <div class="card-block">
                                <div class="card-header card-inverse card-warning" style="text-align: center;">
                                    <h4 style="text-overflow:ellipsis;" data-bind="text: booktitle()+' - p.'+nowpage()"></h4>
                                </div>
                                <img class="card-img" style="text-align: center;width:100%;" data-bind="attr:{src: 'data:image/jpg;base64,'+img()}" />
                                <div class="card-footer card-warning">
                                    <div class="row">
                                        <button type="button" class="btn btn-primary col-xs-6" data-bind="click: function(){$root.viewButtonClicked(title_id(),nowpage());}"><i class="fa fa-caret-right"></i></button>
                                        <button type="button" class="btn btn-warning col-xs-3" data-bind="click: function(){$root.viewButtonClicked(title_id(),1);}"><i class="fa fa-angle-double-left"></i></button>
                                        <button type="button" class="btn btn-success col-xs-3" data-bind="click: function(){$root.viewButtonClicked(title_id(),indexpage());}"><i class="fa fa-bookmark"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- ko if:($index() %4 == 3) --><div class="clearfix hidden-md-down"></div><!-- /ko-->
                    <!-- ko if:($index() %3 == 2) --><div class="clearfix hidden-lg-up hidden-xs-down"></div><!-- /ko-->
                    <!-- /ko -->
                </div>
            </div>
            <div class="row" id="thumbs" style="background-color:#acb0d5; ">
                <br>
                <div class="">
                    <!-- ko foreach: thumbs_group -->
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-3 col-xl-3">
                        <div class="card card-primary">
                            <div class="card-block">
                                <div class="card-header card-inverse card-primary" style="text-align: center;">
                                    <h4 data-bind="text: booktitle()+' - p.'+nowpage()"></h4>
                                </div>
                                <img class="card-img" style="text-align: center;width:100%;" data-bind="attr:{src: 'data:image/jpg;base64,'+img()}" />
                                <div class="card-footer card-primary">
                                    <div class="row">
                                        <button type="button" class="btn btn-warning col-xs-6" data-bind="click: function(){$root.viewButtonClicked(title_id(),1);}"><i class="fa fa-angle-double-left"></i></button>
                                        <button type="button" class="btn btn-success col-xs-6" data-bind="click: function(){$root.viewButtonClicked(title_id(),indexpage());}"><i class="fa fa-bookmark"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- ko if:($index() %4 == 3) --><div class="clearfix hidden-md-down"></div><!-- /ko-->
                    <!-- ko if:($index() %3 == 2) --><div class="clearfix hidden-lg-up hidden-xs-down"></div><!-- /ko-->
                    <!-- /ko -->
                </div>
            </div>
        </span>
    <button id="pageTopL" style="position:fixed;bottom:3%;left:5%" type="button" class="btn btn-warning"><i class="fa fa-caret-up" style="display: block;"></i></button>
    <button id="pageTopR" style="position:fixed;bottom:3%;right:5%" type="button" class="btn btn-warning"><i class="fa fa-caret-up" style="display: block;"></i></button>
</form>
</body>
</html>
