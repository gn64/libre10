<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="ja-JP" xml:lang="ja-JP">
<!--
# Libre10 web based pdf search engine
# Copyright (C) 2013-16 yukikaze
-->
<head>
    <meta charset="utf-8"/>
	<title data-bind="text: titlestr"></title>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/tether/1.1.1/css/tether.min.css">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <style type="text/css">
        @page {
            size: landscape;
        }
        @media print {
            html, body {
                width:100%;
                height:100%;
                margin:0px;
                padding:0px;
            }
            #header{
                display: none;
            }
            #footer{
                display: none;
            }
            #image {
                width: 100%;
                height: 100%;
                margin-left: auto;
                margin-right: auto;
                text-justify: auto;
            }
            #pos {
                display: none;
            }
            form {
                height: 100%;
            }
            img {
                object-fit: contain;
                width:50%;
                max-height: 100%;
            }
        }
    </style>
    <!--[if lt IE 9]>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <![endif]-->
    <!--[if gte IE 9]><!-->
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <!--<![endif]-->
    <script type="text/javascript" src="//ajax.aspnetcdn.com/ajax/knockout/knockout-3.4.0.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/knockout.mapping/2.4.1/knockout.mapping.min.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/hammer.js/2.0.6/hammer.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/jquery.hammerjs/2.0.0/jquery.hammer.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/tether/1.1.1/js/tether.min.js"></script>
    <script type="text/javascript" src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/js-cookie/2.1.0/js.cookie.min.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=yes,maximum-scale=5.0" />
    <meta http-equiv="Expires" content="3600">
    <link rel="shortcut icon" href="{{get_url('/')}}images/favicon.ico" type="image/x-icon">
    <link rel="icon" href="{{get_url('/')}}images/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" sizes="57x57" href="{{get_url('/')}}images/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="{{get_url('/')}}images/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="{{get_url('/')}}images/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="{{get_url('/')}}images/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="{{get_url('/')}}images/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="{{get_url('/')}}images/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="{{get_url('/')}}images/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="{{get_url('/')}}images/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="{{get_url('/')}}images/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="{{get_url('/')}}images/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="{{get_url('/')}}images/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="{{get_url('/')}}images/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="{{get_url('/')}}images/favicon-16x16.png">
    <link rel="manifest" href="{{get_url('/')}}images/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{get_url('/')}}images/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
	<script type="text/javascript">
		$(function(){
            var searchResults, searchtext, title_id, page_num, search_title_id_now, search_page_num_now, speed_init;
            var preCache=[];
            searchResuls = [];
            function removeItemFromStorage(keystr){
                if (Modernizr.localstorage) {
                    return localStorage.removeItem(keystr);
                } else {
                    Cookies.remove("libre10-"+keystr);
                }
            }
            function setItemToStorage(keystr,value){
                if (Modernizr.localstorage){
                    return localStorage.setItem(keystr, value);
                }else{
                    Cookies.set("libre10-"+keystr,value,{expires:7});
                }
            }
            function getItemFromStorage(keystr){
                if (Modernizr.localstorage){
                    return localStorage.getItem(keystr);
                }else{
                    return Cookies.get("libre10-"+keystr);
                }
            }
            function checkSession(){
                if(getItemFromStorage("session")){//セッションあり
                    session_id = getItemFromStorage("session");
                    console.log("session is there.");
                    $.ajaxSetup({
                        headers: { "X-LIBRE10-SESSION": session_id }
                    });
                    posting = $.post('{{get_url('/')}}sessionval',{session: session_id},function(data){
                        isSessionSigned = ( data.result == 1 );
                        if (! isSessionSigned) {//有効なセッションでない場合
                            history.pushState('','',window.location.href);
                            window.location.href = "{{get_url('/')}}login.html";
                        }else{
                            console.log("session validated.");
                            $.ajaxSetup({
                                headers: { "X-LIBRE10-SESSION": session_id }
                            });
                        }
                    },"json");
                    return posting;
                }else{//セッションなしの場合
                    history.pushState('','',window.location.href);
                    window.location.href = "{{get_url('/')}}login.html";
                }
            }
            if (! getItemFromStorage("speedx")){
                setItemToStorage("speedx",-1);
                speed_init = -1;
            }else{
                speed_init = getItemFromStorage("speedx");
            }
            var ua=window.navigator.userAgent.toLowerCase();
            var isMobile = (ua.indexOf('android') > 0 || ua.indexOf('iphone') > 0 ||
                    ua.indexOf('ipad') > 0 || ua.indexOf('tablet') > 0)? 1:0;
            //isMobile = 1;//fix me
            var image_width_size;
            var defaultJS={"booktitle":"","img1":"","img2":"",
                "homelink":"","indexpage":"","jumplink":"","toplink":"{{get_url('/')}}index.html",
                "maxpage":"0","nowpage":"0","nowspd":"0",
                "indexnum":0, "show_alert_loading":0, "show_speed_select":0,
                "light":isMobile,"searchtext" :"","pos":[]
            };
            var width;
            var speed_x = 1;
            if (speed_init < 0){
                speed_x = 1;
            }else{
                speed_x = speed_init;
            }
            var btitle_id, ntitle_id, bpage = -1, npage =0;
            var last_access_title, last_access_pagenum=0;
            var vim_number_cache=0;
            var shift_key_press=0;
            var nextPage, lastPage, nextSearchPage, lastSearchPage;
            var ajaxRequests = [];
			$(document).ready(function(){
                var retinaSwitch = (window.devicePixelRatio > 1)? window.devicePixelRatio : 1 ;
				var image1 = jQuery("#image1"), image2 = jQuery("#image2");
                if (isMobile != 1){
                    $('span[rel=tooltip]').tooltip();
                }
                //knockdown.js
                function myViewModel(data){
                    var self = this;
                    ko.mapping.fromJS(data,{},self);
                    self.light = ko.observable(isMobile);
                    image_width_size = ko.observable(49*(1 + self.light()));
                    self.jpeg_width = ko.computed(function(){
                        return image_width_size() + "%";
                    },self);
                    self.page_text = ko.computed(function(){
                        var N_spd = Number(self.nowspd());
                        var N_nowpage = Number(self.nowpage());
                        if (Number(self.nowspd()) > 0){
                            return String(N_nowpage-N_spd)+"("+self.nowpage()+")"
                        }else{
                            return self.nowpage()
                        }
                    },self);
                    self.jumppage = ko.observable();
                    self.searchtext = ko.computed(function(){return searchtext});
                    self.image_width = ko.observable();
                    self.image_height = ko.observable();
                    self.titlestr = ko.computed(function(){
                        return "page-"+self.page_text()+" "+self.booktitle()+"|Libre10"
                    },self);
                    var show_pos = 1;
                    if (getItemFromStorage("show_pos")==null){
                        show_pos =1;
                        setItemToStorage("show_pos",1);
                    }else{
                        show_pos = Number(getItemFromStorage("show_pos"));
                    }
                    self.show_pos_highlighter = ko.observable(show_pos);
                    self.get_pos_top= function(top){
                        return ko.computed(function(){
                            return ($("#nav-top").height()/1.5) + (top * self.image_height()/100) + 'px';
                                });
                    };
                    self.get_pos_width= function(width){
                        return ko.computed(function(){
                            var t_width=25;
                            if (width>25){t_width = width;}
                            return self.image_width() * (t_width + 1) /100+'px';
                        });
                    };
                    self.get_pos_left= function(left){
                        return ko.computed(function(){
                            return left * self.image_width() / 100 + 'px';
                        });
                    };
                    self.get_pos_height= function(height){
                        return ko.computed(function(){
                            var t_height =$("#nav-top").height();
                            var n_height = self.image_height() * (height+1) /100;
                            if (n_height > t_height){t_height = n_height;}
                            return t_height+'px';
                        });
                    };
                    self.showWordPosition = function(){
                        if(self.show_pos_highlighter() == 1){
                            self.show_pos_highlighter(0);
                            setItemToStorage("show_pos",0);
                        }else{
                            self.show_pos_highlighter(1);
                            setItemToStorage("show_pos",1);
                        }
                    };
                    self.showPrintPage = function() {
                        window.print();
                    };
                    self.moveNextPage = function(addpage){
                        abortConnections();
                        if (addpage == null) addpage = 1;
                        var newpage = Number(self.nowpage()) - self.light() + 1 + addpage ;
                        self.nowpage(newpage);
                        updateJSONpost(title_id,newpage);
                        cacheNextPage();
                    };
                    nextPage=self.moveNextPage;
                    self.moveLastPage = function(minuspage){
                        abortConnections();
                        if (minuspage == null) minuspage = 1;
                        var newpage = Number(self.nowpage()) + self.light() - 1 - minuspage;
                        self.nowpage(newpage);
                        updateJSONpost(title_id,newpage);
                        cacheLastPage();
                    };
                    lastPage=self.moveLastPage;
                    self.moveNextSearchPage = function(){
                        if (npage > 0){
                            abortConnections();
                            self.nowpage(npage);
                            updateJSONpost(ntitle_id,npage);
                            setSearchVars(ntitle_id,npage);
                            setSearchJump();
                            cacheNextPage();cacheLastPage();
                            cacheNextSearchPage();cacheLastPage();
                        }
                    };
                    nextSearchPage = self.moveNextSearchPage;
                    self.moveLastSearchPage = function(){
                        if (bpage > 0){
                            abortConnections();
                            self.nowpage(bpage);
                            updateJSONpost(btitle_id,bpage);
                            setSearchVars(btitle_id,bpage);
                            setSearchJump();
                            cacheNextPage();cacheLastPage();
                            cacheNextSearchPage();cacheLastPage();
                        }
                    };
                    lastSearchPage = self.moveLastSearchPage;
                    self.moveIndexPage = function(){
                        if (Number(self.indexpage()) > 0){
                            abortConnections();
                            self.nowpage(self.indexpage());
                            updateJSONpost(title_id, Number(self.indexpage()));
                            cacheNextPage();cacheLastPage();
                        }
                    };
                    self.jumpToPage = function(){
                        abortConnections();
                        var jnum=Number(self.jumppage())+Number(self.nowspd());
                        $("#jumpModal").modal('hide');
                        self.nowpage(jnum);
                        updateJSONpost(title_id,jnum);
                        cacheNextPage();cacheLastPage();
                    };
                    self.jumpKeyPress = function(place,event){
                        if(event.keyCode == 13){
                            event.target.blur();
                            self.jumpToPage();
                        }
                        return true;
                    };
                    self.setPDF = function(){
                        var spd="",idp="";
                        if(self.nowspd() > 0){
                            spd=self.nowspd();
                        }
                        if(self.indexpage() > 0){
                            idp=self.indexpage();
                        }
                        $.ajax({
                            type: 'POST',
                            url: '{{get_url('/')}}libre10.wsgi/manage',
                            cache: false,
                            data: {
                                session:getItemFromStorage("session"),
                                mode:"setpdfdesc",
                                titleid:title_id,
                                spd:spd,
                                indexpage:idp
                            },
                            complete: function(data){
                                $("#settingModal").modal('hide');
                            }
                        });

                    };
                    self.bodyKeyDown = function(p,e){
                        //console.log(e.keyCode);
                        switch (e.keyCode) {
                            /*
                                next : d 68, right 39, l 76
                                last a 65, left 37, h 72
                                nextSearch : n, d(with shift), right(with shift)
                                LastSearch: N(Shift+n), a(with shift), left(with shift)
                                show 1page e 69, + 107/187, > 190 (with Shift)
                                show 2page (in dual page mode.) q 81, - 109/189, < 188 (with Shift)
                                up w 87, up 38, k 75
                                down s 83, down 40, j 74
                                Reset <Num> Esc 27
                                <Num>jump g 71
                            */
                            case 76:// Key: l
                                if (shift_key_press==1){
                                    break;
                                }
                            case 39:// Key: right
                            case 68:// Key: d
                                if (shift_key_press==0){
                                    if (vim_number_cache != 0){
                                        self.moveNextPage(vim_number_cache);
                                    }else{
                                        self.moveNextPage();
                                    }
                                }else if(shift_key_press==1){
                                    self.moveNextSearchPage();
                                }
                                vim_number_cache = 0;
                                break;
                            case 72:// Key: h
                                if (shift_key_press==1){
                                    break;
                                }
                            case 37:// Key: left
                            case 65:// Key: a
                                if (shift_key_press==0) {
                                    if (vim_number_cache != 0) {
                                        self.moveLastPage(vim_number_cache);
                                    } else {
                                        self.moveLastPage();
                                    }
                                }else if(shift_key_press==1){
                                    self.moveLastSearchPage();
                                }
                                vim_number_cache = 0;
                                break;
                            case 188:// Key:<
                                if (shift_key_press==0){
                                    break;
                                }
                            case 81:// Key: q
                            case 109:// Key: -
                            case 189:// Key: -
                                if (isMobile != 1) {
                                    image_width_size(49);
                                    image1.stop(true).animate({width: image_width_size + "%"}, 250);
                                    image2.stop(true).animate({width: image_width_size + "%"}, 250);
                                    $(window).resize();
                                }
                                vim_number_cache = 0;
                                break;
                            case 190:// Key: >
                                if (shift_key_press==0){
                                    break;
                                }
                            case 69:// Key: e
                            case 107:// Key: +
                            case 187:// Key: +
                                if (isMobile != 1) {
                                    image_width_size(98);
                                    image1.stop(true).animate({width: image_width_size + "%"}, 250);
                                    image2.stop(true).animate({width: image_width_size + "%"}, 250);
                                    $(window).resize();
                                }
                                vim_number_cache = 0;
                                break;
                            case 38:// Key: up
                            case 87:// Key: w
                            case 75:// Key: k
                                var pointTop=$(window).scrollTop();
                                var pointHeight= $(window).height();
                                var newPointTop=pointTop - pointHeight*0.9 ;
                                if (newPointTop < 0){newPointTop=0;}
                                $("body,html").animate({scrollTop:newPointTop},'fast');
                                vim_number_cache = 0;
                                break;
                            case 40:// Key: down
                            case 83:// Key: s
                            case 74:// Key: j
                                var pointTop=$(window).scrollTop();
                                var pointHeight= $(window).height();
                                var newPointTop=pointTop + pointHeight*0.9;
                                $("body,html").animate({scrollTop:newPointTop},'fast');
                                vim_number_cache = 0;
                                break;
                            case 27:// Key: Esc
                                vim_number_cache = 0;
                                break;
                            case 71:// Key: g
                                if (shift_key_press == 1){
                                    if (vim_number_cache >0){
                                        var jnum=vim_number_cache + Number(self.nowspd());
                                        updateJSONpost(title_id,jnum);
                                    }
                                }
                                vim_number_cache=0;
                                break;
                            case 191:// Key: /
                                window.open(self.toplink(),'_blank');
                                vim_number_cache=0;
                                break;
                            case 16:// Key: Shift
                                shift_key_press = 1;
                                break;
                            case 78:// Key: n/N
                                if (vim_number_cache>0){
                                    if (shift_key_press == 0){
                                        for (var i=0;i < vim_number_cache-1;i++) {
                                            setSearchVars(ntitle_id, npage);
                                            setSearchJump();
                                        }
                                        self.moveNextSearchPage();
                                    }else if (shift_key_press == 1){
                                        for (var i=0;i < vim_number_cache-1;i++){
                                            setSearchVars(btitle_id,bpage);
                                            setSearchJump();
                                        }
                                        self.moveLastSearchPage();
                                    }
                                }else {
                                    if (shift_key_press == 0) {
                                        self.moveNextSearchPage();
                                    } else if (shift_key_press == 1) {
                                        self.moveLastSearchPage();
                                    }
                                }
                                vim_number_cache = 0;
                                break;

                        }
                        if ((e.keyCode >= 48)&&(e.keyCode <= 57)){
                            var cache_str = vim_number_cache.toString();
                            cache_str = cache_str + (e.keyCode-48).toString();
                            vim_number_cache = Number(cache_str);
                            if (vim_number_cache > 99999){
                                vim_number_cache = 0;
                            }
                        }
                        return true;
                    };
                    self.bodyKeyUp = function(p,e){
                        //console.log(e.keyCode);
                        switch (e.keyCode) {
                            /*
                                page switch(1p/2p) : Shift 16
                            */
                            case 16:// Key: shift
                                shift_key_press=0;
                                break;
                        }
                        return true;
                    };
                    self.showPageDiff = function(place,event){
                        var newpage = Number(self.nowpage())+Number(event.target.text.replace("p",""));
                        updateJSONpost(title_id,newpage);
                    };
                    self.hideAlert = function(){
                        self.show_alert_loading(0);
                    };window.hideAlert=self.hideAlert;
                    self.showAlert = function(){
                        self.show_alert_loading(1);
                    };window.showAlert=self.showAlert;
                    self.showSpeedSelect = function(){
                        self.show_speed_select(1);
                    };window.showSpeedSelect= self.showSpeedSelect;
                    self.returnParent = function(){
                        if (!window.opener || window.opener.closed){
                            window.location.replace(self.toplink());
                        }else{
                            window.opener.focus();
                            window.close();
                        }
                    };
                    self.setResolution= function(x){
                        speed_x = x;
                        if (x == 1){
                            speed_init = -1;
                            setItemToStorage("speedx",-1);
                            self.show_speed_select(0);
                        }else{
                            if (speed_init == -1){
                                self.show_speed_select(0);
                            }else{
                                self.show_speed_select(1);
                                setItemToStorage("speedx", x);
                            }
                        }
                        window.updateJSONpost(last_access_title,last_access_pagenum);
                    };
                }
                function setSearchJump(){
                    var temp_btitle, temp_bpage=0;
                    bpage = -1, npage =0;
                    if (searchResults){
                        if (searchResults.length > 0){
                            for (var i= 0; i < searchResults.length; i++){
                                var now_group = searchResults[i];
                                for (var j = 0; j < now_group['search_result'].length;j++){
                                    var now_page = now_group['search_result'][j];
                                    if (bpage != -1){
                                        ntitle_id = now_page['title'];
                                        npage = now_page['page'];
                                        i = searchResults.length;
                                        j = now_group['search_result'].length;
                                    }
                                    if ((now_page['page'] === search_page_num_now)&&(now_page['title'] === search_title_id_now)){
                                        if (temp_bpage != 0) {
                                            bpage = temp_bpage;
                                            btitle_id = temp_btitle;
                                        }else{
                                            bpage = now_page['page'];
                                            btitle_id = now_page['title'];
                                        }
                                        if((i == searchResults.length-1)&&(j == now_group['search_result'].length-1)){
                                            npage = now_page['page'];
                                            ntitle_id = now_page['title'];
                                        }
                                    }
                                    temp_bpage = now_page['page'];
                                    temp_btitle = now_page['title'];
                                }
                            }
                        }
                    }
                }window.setSearchJump = setSearchJump;
                function updateWindowSize(){
                    width = retinaSwitch * $(window).width();
                }
                function setSearchVars(title,pagenum){
                    search_page_num_now = pagenum;
                    search_title_id_now = title;
                }window.setSearchVars = setSearchVars;
                function JSONpost(title,pagenum,isCache){
                    if (!isCache){
                        self.showAlert();
                    }
                    var defer = $.Deferred();
                    title_id = title;
                    page_num = pagenum;
                    var mode_str=(isMobile == 1)? "light":"";
                    updateWindowSize();
                    var tmp_width=(isMobile == 1)? width : Math.floor(width * (0.5+0.2 * (image_width_size()/49-1)));
                    tmp_width = (tmp_width < 900) ? 900 : tmp_width ;
                    tmp_width = tmp_width * speed_x;
                    last_access_pagenum = pagenum;
                    last_access_title = title;
                    view_address_str = "{{get_url('/')}}view/"+title+"/"+pagenum+"/";
                    window.history.pushState(null, "page-"+pagenum+" / Libre10", view_address_str);
                    address_str='{{get_url('/')}}getimage/'+tmp_width+"/"+title+"/"+pagenum+"/"+isMobile+"/";
                    var haveCache = false;
                    var CacheJson = {'json':[]};
                    if (isCache){
                        CacheJson = {'url':view_address_str};
                    }
                    //console.log(preCache);
                    if (preCache){
                        for (var i=0 ;i<preCache.length;i++){
                            if (preCache[i]){
                                if (preCache[i]['url']==view_address_str){
                                    haveCache = true;
                                    CacheJson = preCache[i]['json'];
                                    if (isCache){
                                        preCache.splice(i,1);
                                    }
                                }
                            }
                        }
                    }
                    if (haveCache){
                        //console.log('haveCache!');
                        if (isCache){

                        }else{
                            ko.mapping.fromJS(CacheJson,viewModel);
                        }
                        defer.resolve();
                    }else{
                        var startTime = new Date().getTime();
                        var gj = $.ajax({
                            type: 'GET',
                            url: address_str,
                            cache: false,
                            /*data: {
                             session: getItemFromStorage("session"),
                             mode : mode_str,
                             width : tmp_width,
                             pagenum : pagenum,
                             title : title
                             },*/
                            dataType: "json",
                            timeout: (isMobile == 1)? 12000 : 8000, // in milliseconds
                            success: function(data){
                                if (isCache){
                                    CacheJson['json'] = data;
                                }else{
                                    ko.mapping.fromJS(data,viewModel);
                                }
                                var request_time = new Date().getTime() - startTime;
                                if (request_time > 5000){
                                    if (speed_x == 1){
                                        speed_x = 0.7;
                                    }else if (speed_x == 0.7){
                                        speed_x = 0.5;
                                    }else if (speed_x == 0.5){
                                        //
                                    }
                                }else if (request_time < 1000){
                                    if (speed_x == 1){
                                        //
                                    }else if (speed_x == 0.7){
                                        speed_x = 1;
                                    }else if (speed_x == 0.5){
                                        speed_x = 0.7;
                                    }
                                }
                                //console.log(speed_x);
                                //console.log(request_time);
                            },
                            error: function(XMLHttpRequest, textStatus) {
                                if (textStatus == "timeout"){
                                    window.showSpeedSelect();
                                }
                                window.hideAlert();
                            }
                        });
                        gj.done(function(){
                            ajaxRequests.splice(ajaxRequests.indexOf(gj),1);
                            window.hideAlert();
                            if (searchtext) {
                                address_str = '{{get_url('/')}}getpos/' + title + "/" + pagenum + "/" + searchtext + "/";
                                var gj2 = $.ajax({
                                    type: 'GET',
                                    url: address_str,
                                    cache: false,
                                    /*data: {
                                     session: getItemFromStorage("session"),
                                     width : tmp_width,
                                     pagenum : pagenum,
                                     title : title,
                                     text : searchtext
                                     },*/
                                    dataType: "json",
                                    timeout: (isMobile == 1) ? 10000 : 8000, // in milliseconds
                                    success: function (data) {
                                        if (isCache){
                                            //CacheJson['json'] = CacheJson['json'].concat(data);
                                            CacheJson['json'] = $.extend(CacheJson['json'],data);
                                        }else{
                                            ko.mapping.fromJS(data, viewModel);
                                            $(window).resize();
                                            ajaxRequests.splice(ajaxRequests.indexOf(gj),1);
                                        }
                                    }
                                });
                            }
                        }).done(function(){defer.resolve();});
                        gj.fail(function(){
                            if (ajaxRequests.indexOf(gj)>-1){
                                ajaxRequests.splice(ajaxRequests.indexOf(gj),1);
                            }
                        });
                        ajaxRequests.push(gj);
                    }
                    if (isCache){
                        preCache.push(CacheJson);
                        if (preCache.length > 30){
                            preCache.shift();
                        }
                    }
                    return defer.promise();
                }
                function updateJSONpost(title,pagenum){
                    var defer = $.Deferred();
                    JSONpost(title,pagenum,false).done(function(){
                        return defer.resolve();
                    });
                    return defer.promise();
                }window.updateJSONpost = updateJSONpost;
                function cacheJSONpost(title,pagenum){
                    var defer = $.Deferred();
                    JSONpost(title,pagenum,true).done(function(){
                        return defer.resolve();
                    });
                    return defer.promise();
                }window.cacheJSONpost = cacheJSONpost;
                function abortConnections(){
                    ajaxRequests.forEach(function(val,index,ar){
                        val.abort();
                        ajaxRequests.splice(ajaxRequests.indexOf(val),1);
                    });
                }
                function setSearchResults(data,get_searchtext){
                    searchResults = data;
                    searchtext = get_searchtext;
                }window.setSearchResults = setSearchResults;
                function cacheNextPage(){
                    var defer = $.Deferred();
                    var newpage = Number(page_num) + 1 ;
                    cacheJSONpost(title_id,newpage).done(function(){
                        cacheJSONpost(title_id,newpage+1).done(function(){
                            return defer.resolve();
                        });
                    });
                    return defer.promise();
                };
                function cacheLastPage(){
                    var defer = $.Deferred();
                    var newpage = Number(page_num) - 2;
                    cacheJSONpost(title_id,newpage).done(function(){
                        cacheJSONpost(title_id,newpage+1).done(function(){
                            return defer.resolve();
                        });
                    });
                    return defer.promise();
                };
                function cacheNextSearchPage(){
                    var defer = $.Deferred();
                    if (npage>0){
                        cacheJSONpost(title_id,npage).done(function(){
                            cacheJSONpost(title_id,npage+1).done(function(){
                                return defer.resolve();
                            });
                        });
                    }else{
                        return defer.resolve();
                    }
                    return defer.promise();
                };
                function cacheLastSearchPage(){
                    var defer = $.Deferred();
                    if (bpage>0){
                        cacheJSONpost(title_id,bpage).done(function(){
                            cacheJSONpost(title_id,bpage-1).done(function(){
                                return defer.resolve();
                            });
                        });
                    }else{
                        return defer.resolve();
                    }
                    return defer.promise();
                };
                var viewModel=new myViewModel(defaultJS);
                //page_num = "{{pagenum}}";
                checkSession().done(updateJSONpost("{{title}}","{{pagenum}}").done(function(){
                    cacheNextPage();cacheLastPage();
                    cacheNextSearchPage();cacheLastPage();
                }));
                ko.applyBindings(viewModel, document.getElementById("htmlTop"));
                var swipeOptions = {};
                $("#image").hammer(swipeOptions).bind("swipeleft",function(ev){
                    ev.preventDefault();
                    nextPage();
                });
                $("#image").hammer(swipeOptions).bind("swiperight",function(ev){
                    ev.preventDefault();
                    lastPage();
                });
                $("#image").data('hammer').add(new Hammer.Swipe({event:'doubleswipeleft',pointers:2, direction:Hammer.DIRECTION_LEFT}));
                $("#image").data('hammer').add(new Hammer.Swipe({event:'doubleswiperight',pointers:2, direction:Hammer.DIRECTION_RIGHT}));
                $("#image").hammer(swipeOptions).bind("doubleswipeleft",function(ev){
                    ev.preventDefault();
                    nextSearchPage();
                });
                $("#image").hammer(swipeOptions).bind("doubleswiperight",function(ev){
                    ev.preventDefault();
                    lastSearchPage();
                });
                $(window).resize(function(){
                    viewModel.image_height($("#image1").height());
                    viewModel.image_width($("#image1").width());
                });
                //cacheNextPage();cacheLastPage();
			});
		});
	</script>
    <style type="text/css">
        .navbar-center
        {
            width: 100%;
            text-align:center;
        }
    </style>
</head>
<body data-bind="event:{keydown: bodyKeyDown, keyup: bodyKeyUp},submit: function(){;}">
	<div id='header' style="align:center;">
		<nav id='nav-top' class="navbar navbar-dark bg-inverse navbar-full" >
            <a class="navbar-brand" data-bind="click: returnParent">Libre10</a>
            <ul class="nav navbar-nav navbar-dark bg-inverse">
                <li class="nav-item">
                    <a class="nav-link" href="javascript:void(0);" data-bind="click:function(){moveLastPage()}">
                        <i class="fa fa-chevron-left" rel="tooltip" data-toggle="tooltip" data-placement="bottom" title="LastPage (a|←)"></i>
                    </a>
                </li>
                <li class="nav-item">
                    <a class='nav-link' href="javascript:void(0);" data-bind="click:function(){moveLastSearchPage()}">
                        <i class="fa fa-hand-o-left" rel="tooltip" data-toggle="tooltip" data-placement="bottom" title="LastSearch"></i>
                    </a>
                </li>
                <li class="nav-item" class="dropdown">
                    <a href="#" class="nav-link dropdown-toggle" role="button" aria-haspopup="true" expanded="false" data-toggle="dropdown"><i class="fa fa-share-square"></i><b class="caret"></b></a>
                    <div class="dropdown-menu">
                        <!-- ko ifnot: light -->

                        <h6 class="dropdown-header">Special(Shift)</h6>
                        <a class="dropdown-item" href="#" data-bind="click: showPageDiff">+1p</a>
                        <div class="dropdown-divider"></div>
                        <!-- /ko -->
                        <h6 class="dropdown-header">Next</h6>
                        <a class="dropdown-item" href="#" data-bind="click: showPageDiff">+10p</a>
                        <a class="dropdown-item" href="#" data-bind="click: showPageDiff">+20p</a>
                        <a class="dropdown-item" href="#" data-bind="click: showPageDiff">+50p</a>
                        <a class="dropdown-item" href="#" data-bind="click: showPageDiff">+100p</a>
                        <h6 class="dropdown-header">Back</h6>
                        <a class="dropdown-item" href="#" data-bind="click: showPageDiff">-100p</a>
                        <a class="dropdown-item" href="#" data-bind="click: showPageDiff">-50p</a>
                        <a class="dropdown-item" href="#" data-bind="click: showPageDiff">-20p</a>
                        <a class="dropdown-item" href="#" data-bind="click: showPageDiff">-10p</a>
                    </div>
                </li>
                <li class="nav-item pull-left">
                    <a class="nav-link" href="#jumpModal" data-toggle="modal" data-target="#jumpModal">
                        <i class="fa fa-rocket" rel="tooltip" data-toggle="tooltip" data-placement="bottom" title="Jump"></i>
                    </a>
                </li>
                <li class="nav-item pull-xs-right"><a class="nav-link" href="javascript:void(0);" data-bind="click:function(){moveNextPage(1)}">
                    <i class="fa fa-chevron-right"
                          rel="tooltip" data-toggle="tooltip" data-placement="bottom" title="NextPage (d|→)"></i>
                </a></li>
                <li class="nav-item pull-xs-right"><a class="nav-link" href="javascript:void(0);" data-bind="click:function(){moveNextSearchPage()}">
                    <i class="fa fa-hand-o-right"
                       rel="tooltip" data-toggle="tooltip" data-placement="bottom" title="NextSearch"></i>
                </a></li>
                <li class="nav-item pull-xs-right" data-bind="visible:indexpage"><a class="nav-link" href="javascript:void(0);" data-bind="click:moveIndexPage">
                    <i class="fa fa-bookmark" rel="tooltip" data-toggle="tooltip" data-placement="bottom" title="IndexPage"></i>
                </a></li>
                <li class="nav-item pull-xs-right"><a class="nav-link" href="#settingModal" data-toggle="modal" data-target="#settingModal">
                    <i class="fa fa-tasks"
                       rel="tooltip" data-toggle="tooltip" data-placement="bottom" title="PDFSetting"></i>
                </a></li>
                <li class="nav-item pull-xs-right"><a class="nav-link" href="{{get_url('/')}}help/usage_search_en.html#view-screen" target="_blank">
                    <i class="fa fa-question-circle" rel="tooltip" data-toggle="tooltip" data-placement="bottom" title="help"></i>
                </a></li>
            </ul>
            <form class="form-inline navbar-form navbar-center">
                <h6 class="form-control bg-inverse">
                    <span data-bind="text: booktitle"></span>
                    <span data-bind="text: page_text"></span>
                </h6>

                <!--<h6 class="navbar-text" data-bind="text: page_text" style="float:none;display:inline-block;"></h6>-->
                <!--<h5 class="navbar-text" data-bind="text: booktitle" style="float:none;display:inline-block;"></h5>-->
            </form>
		</nav>
        <div id="jumpModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="jumpModalTitle" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" aria-hidden="true" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title" id="jumpModalTitle">Jump page</h4>
                    </div>
                    <div class="modal-body">
                        <input type="number" class="form-control" placeholder="Page"
                               name="jumpPageNum" id="jumpPageNum" min="1" id="jumpModalInput" autofocus
                               data-bind="value: jumppage,event:{keypress: jumpKeyPress}"/>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" data-bind="click: jumpToPage" data-dismiss="modal">Jmp</button>
                    </div>
                </div>
            </div>
        </div>
        <div id="settingModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="settingModalTitle" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" aria-hidden="true" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title" id="settingModalTitle">PDF setting</h4>
                    </div>
                    <div class="modal-body">
                        <h4>PDF page number minus page number in books :</h4>
                            <input type="number" class="form-control" placeholder="Page" name="settingSPDNum"
                                   id="settingSPDNum" min="0" data-bind="value: nowspd"/>
                        <h4>Index page (in pdf page number) :</h4>
                            <input type="number" class="form-control" placeholder="Page" name="settingIndexNum"
                                   id="settingIndexNum" min="0" data-bind="value: indexpage"/>
                        <h5>PDF page number might be shown like "books_page_number(pdf_page_number)" or "pdf_page_number"</h5>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" data-bind="click: setPDF" data-dismiss="modal">Set</button>
                    </div>
                </div>
            </div>
        </div>
	</div>
	<form method="get" id="main_form">
        <span  data-bind="visible: show_alert_loading">
            <div class="alert alert-warning span12" role="alert" align="center" style="align:center;position:absolute;top:0%;width:100%;z-index:2;">
                <span class="glyphicon glyphicon-refresh"></span>
                Now loading...
                <span class="glyphicon glyphicon-refresh"></span>
            </div>
        </span>
        <span  data-bind="visible: show_speed_select">
            <div class="alert alert-info " role="alert" align="center" style="align:center;width:100%;z-index:2;margin:0;">
                Change resolution?
                <a href="#" class="alert-link" data-bind="click:function(){setResolution(1)}">Default</a>
                <a href="#" class="alert-link" data-bind="click:function(){setResolution(0.7)}">Fast</a>
                <a href="#" class="alert-link" data-bind="click:function(){setResolution(0.5)}">Ultrafast</a>
            </div>
        </span>
        <div id="pos" data-bind="visible: show_pos_highlighter">
            <div data-bind="foreach: pos">
                <img class="img-rounded bg-warning" style="opacity:0.4;position:absolute;z-index:2"
                     data-bind="style:{
                    top: $root.get_pos_top(top()),
                    left: $root.get_pos_left(left()),
                    width: $root.get_pos_width(width()),
                    height:$root.get_pos_height(height())}
                    ">
            </div>
        </div>
		<div id='image'>
            <!-- ko if: light -->
            <img id='image1' class="img-responsive"
                 data-bind="attr:{src: 'data:image/jpg;base64,'+img1(),width: jpeg_width}" />
            <!-- /ko -->
            <!-- ko ifnot: light -->
            <img id='image1' class="img-responsive" align="left"
                 data-bind="attr:{src: 'data:image/jpg;base64,'+img1(),width: jpeg_width}" />
            <img id='image2' class="img-responsive"
                     data-bind="attr:{src: 'data:image/jpg;base64,'+img2(),width: jpeg_width}"/>
            <!-- /ko -->
		</div>
	</form>
    <div class="clearfix"></div>
	<div id="footer">
		<nav class="navbar navbar-dark bg-inverse navbar-full" >
            <ul class="nav navbar-nav navbar-dark bg-inverse">
                <li class="nav-item"><a class="nav-link" href="javascript:void(0);" data-bind="click:function(){moveLastPage(1)}">
                    <i class="fa fa-chevron-left"
                              rel="tooltip" data-toggle="tooltip" data-placement="top" title="Last"></i>
                </a></li>
                <li class="nav-item"><a class="nav-link" href="javascript:void(0);" data-bind="click:function(){moveLastSearchPage()}">
                    <i class="fa fa-hand-o-left"
                              rel="tooltip" data-toggle="tooltip" data-placement="top" title="Last Search"></i>
                </a></li>
                <li class="nav-item dropup">
                    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-compress" rel="tooltip" data-toggle="tooltip"
                           data-placement="top" title="Resolution"></i>
                    </a>
                    <div class="dropdown-menu">
                        <a href="#" class="dropdown-item" data-bind="click:function(){setResolution(1.4)}">Fine(slow)</a>
                        <a href="#" class="dropdown-item" data-bind="click:function(){setResolution(1)}">Normal(Default)</a>
                        <a href="#" class="dropdown-item" data-bind="click:function(){setResolution(0.7)}">Light(Fast)</a>
                        <a href="#" class="dropdown-item" data-bind="click:function(){setResolution(0.5)}">Ultralight(Ultrafast)</a>
                    </div>
                </li>
                <li class="nav-item pull-xs-right"><a class="nav-link" href="javascript:void(0);" data-bind="click:function(){moveNextPage(1)}">
                    <i class="fa fa-chevron-right"
                              rel="tooltip" data-toggle="tooltip" data-placement="top" title="Next"></i>
                </a></li>
                <li class="nav-item pull-xs-right"><a class="nav-link" href="javascript:void(0);" data-bind="click:function(){moveNextSearchPage()}">
                    <i class="fa fa-hand-o-right"
                       rel="tooltip" data-toggle="tooltip" data-placement="top" title="Next Search"></i>
                </a></li>
                <li class="nav-item pull-xs-right"><a class="nav-link" href="javascript:void(0);" data-bind="click:function(){showWordPosition()}">
                    <i class="fa fa-tags"
                       rel="tooltip" data-toggle="tooltip" data-placement="top" title="Word Position"></i>
                </a></li>
                <li class="nav-item pull-xs-right"><a class="nav-link" href="javascript:void(0);" data-bind="click:function(){showPrintPage()}">
                    <i class="fa fa-print"
                       rel="tooltip" data-toggle="tooltip" data-placement="top" title="Print Pages"></i>
                </a></li>
            </ul>
		</nav>
	</div>
</body>
</html>
